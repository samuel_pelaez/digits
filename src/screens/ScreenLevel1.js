import animate
import ui.View as View
import ui.TextView as TextView
import ui.ImageView as ImageView
import ui.ImageScaleView as ImageScaleView
import ui.resource.Image as Image
import event.input.drag as drag;
import src.utils.ClusterDefaultProperties as ClusterDefaultProperties
import src.utils.DraggPinchable as DraggPinchable
import src.utils.Reservoir as Reservoir
import src.utils.InteractionsField as InteractionsField
import src.utils.TileOfDigits as TileOfDigits
import src.utils.ClusterTypes as ClusterTypes
import math.geom.Vec2D as Vec2D
import math.geom.intersect as intersect
import ui.widget.ButtonView as ButtonView

var MAX_N_DRAGGABLES = 30;
var STATUS_PLAYING = 1, STATUS_PAUSED = 0;

exports = Class(View, function (supr) {

	this.init = function (opts) {
		var W = GLOBAL.baseWidth;
		var H = GLOBAL.baseHeight;
		opts = merge(opts, {
			x: 0,
			y: 0,
			width: W,
			height: H,
			backgroundColor:"white"
		});

		supr(this, 'init', [opts]);
		
		ClusterDefaultProperties.def.isLabelVisible = false;
		
		this.reservoir = new Reservoir();
		
		/// Interactions field
		this.field = new InteractionsField({
			superview: this,
			reservoir: this.reservoir,
			x: W*.0, y: H*.00,
			width: W, height: H*.87,
			nClusters:0,
			type: 'units',
			backgroundColor:"#A05050",
		});
		this.field.buttonMove.hide();
		this.field.buttonResize.hide();
		this.field.buttonClear.hide();
		this.clusterTypes = new ClusterTypes();
		this.elementsType = this.clusterTypes.types['units'];
		this.elementsTypeOld = '';
		this.timerDt = 100;
		
		/// Tile of digits
		this.tileOfDigits = new TileOfDigits({
			superview: this,
			x: 0, y: H,
			width: W*.7,
		});
		
		/// Red frame for digits
		this.digitSelector = new ImageScaleView({
			superview: this.tileOfDigits,
			x: this.tileOfDigits.digits[1].style.x,
			y: this.tileOfDigits.digits[1].style.y,
			width: this.tileOfDigits.digits[1].style.width,
			height: this.tileOfDigits.digits[1].style.height,
			image: "resources/images/frame-simple-red.png", 
			scaleMethod: '9slice',
			sourceSlices: {
				horizontal:{left:10,center:44,right:10},
				vertical:{top:10,middle:44,bottom:10}},
			destSlices: {
				horizontal:{left:5,right:5},
				vertical:{top:5,bottom:5}},
		});
		
		this.oldGameTargetN = -1;
		this.build(opts);
		
		/// Begin to play
		animate(this).wait(100).then( bind( this,function(){
			this.reset();
			this.play();
		}));
		
		this.on("OnPlate",bind(this,function(c){this.evaluateSolution(c);}));		
	};
	
	///
	/// build
	///
	this.build = function(opts){
		this.drawGameElements();
		/// Prepare solution dialog
		this.solutionView = new SolutionView({superview: this});
		this.solutionView.hide();
		
		/// Reset button
		var W = GLOBAL.baseWidth;
		var H = GLOBAL.baseHeight;
		var w = h = H*.15;
		this.buttonReset = new ButtonView({
			superview: this,
			x: this.field.style.x + this.field.style.width - w*1.1,
			y: this.field.style.y + h*.1,
			width: w, height: h,
			images:{
				down: "resources/images/icons/refresh.png",
				up: "resources/images/icons/refresh.png",
				disabled: "resources/images/icons/refresh.png",
			},
			on:{
				up: bind(this,"reset"),
			} 
		});
		this.buttonBack = new ButtonView({
			superview: this,
			x: this.field.style.x + w*.1,
			y: this.field.style.y + h*.1,
			width: w, height: h,
			images:{
				down: "resources/images/icons/left_arrow.png",
				up: "resources/images/icons/left_arrow.png",
				disabled: "resources/images/icons/left_arrow.png",
			},
			on:{
				up: bind(this,function(){console.log("emitting exitScreen");this.emit("exitScreen");}),
			} 
		});
	};
	
	///
	/// drawGameElements
	///
	this.drawGameElements = function(){
		var W = this.field.style.width;
		var H = this.field.style.height;
		var x,y,w,h;
		
		/// Draw basket's background
		w = W*20/100.; h = w*1.1;
		x = W*.05; y = H*.5;
		this.basket0 = new ImageView({
			superview: this.field,
			x: x, y: y, width: w, height: h,
			//backgroundColor: "red", opacity: 0.5, // visual guide
			image: "resources/images/level1/basket0.png"
		});
		
		/// Draw basket's foreground, copying style from basket's background
		w = this.basket0.style.width;	h = this.basket0.style.height;
		x = this.basket0.style.x; 		y = this.basket0.style.y;
		this.basket1 = new ImageView({
			superview: this,
			x: x, y: y, width: w, height: h,
			//backgroundColor: "red", opacity: 0.5, // visual guide
			image: "resources/images/level1/basket1.png",
			zIndex: 1
		});
		this.basket1.setHandleEvents(false);
		
		/// Draw empty plate
		w = W*.45;	h = w*238/800.;
		x = W-w*1.2; 		y = H*.45;
		this.plateShadow = new View({
			superview: this.field,
			x: x, y: y, width: w, height: h,
			backgroundColor: "gray",
			opacity: 0.5,
			visible: false
		});
		this.plate = new ImageView({
			superview: this.field,
			x: x, y: y, width: w, height: h,
			//backgroundColor: "red", opacity: 0.5, // visual guide
			image: "resources/images/level1/plate.png"
		});
		
		/// Draw target image
		h = this.field.style.height - this.plate.style.y - this.plate.style.height;
		w = h*this.tileOfDigits.digits[1].style.width/this.tileOfDigits.digits[1].style.height;
		x = this.plate.style.x + this.plate.style.width/2. - w/2.;
		y = this.plate.style.y + this.plate.style.height;
		this.targetImage = new ImageView({
			superview: this,
			x: x, y: y,
			width: w, height: h
		});
		
	};
	
	///
	/// reset
	///
	this.reset = function(){
		console.log("running reset");
		do{
			this.gameTargetN = Math.floor((Math.random()*9)+1);
		}while( this.gameTargetN === this.oldGameTargetN );
		this.oldGameTargetN = this.gameTargetN;
		this.elementsType.image.setURL("resources/images/apple.png");
		this.elementsType.superType = 'none';
		this.elementsType.nMaxElements = 20;
		/// put clusters
		this.field.deleteAllClusters();
		this.spreadClustersWithinContainer();
		/// set the target image
		this.targetImage.setImage( this.tileOfDigits.digits[this.gameTargetN].getImage() );
		/// update the selector on tile-of-digits
		animate(this.digitSelector).now({x: this.tileOfDigits.digits[this.gameTargetN].style.x},800,animate.easeIn);
	};
		
	///
	/// play
	///
	this.play = function(){
		console.log("running play");
		this.buttonReset.setState(ButtonView.states.UP);
		this.buttonBack.setState(ButtonView.states.UP);
		/// Resume animations, if any
		for( var i=0; i<this.field.clusters.length; i++ ){
			var c = this.field.clusters[i];
			c.setHandleEvents(true);
			animate(c).resume();
		}
		/// update status
		this.status = STATUS_PLAYING;
		/// run game timer
		this.setGameTimer();
	};
	
	///
	/// pause
	///
	this.pause = function(excludeCluster){
		console.log("running pause");
		this.buttonReset.setState(ButtonView.states.DISABLED);
		this.buttonBack.setState(ButtonView.states.DISABLED);
		/// Stop animations, if any
		for( var i=0; i<this.field.clusters.length; i++ ){
			var c = this.field.clusters[i];
			var anim = animate(c);
			if( anim.hasFrames() && c !== excludeCluster ){
				anim.pause();
			}
			c.setHandleEvents(false);
		}
		/// update status
		this.status = STATUS_PAUSED;
		/// pause game timer
		clearInterval(this.gameTimer);
	};
	
	///
	/// setGameTimer
	///
	this.setGameTimer = function(){
		this.gameTimer = setInterval(bind(this,function(){
			var overPlate = false;
			for(var i=0; i<this.field.clusters.length; i++ ){
				var c = this.field.clusters[i];
				if( this.detectClusterOnPlate(c) ){
					overPlate = true;
					if( !c.isDragging() && !animate(c).hasFrames() ){
						console.log("On plate");
						this.emit("OnPlate",c);
					}
				}
			}
			if( overPlate ) this.plateShadow.show();
			else this.plateShadow.hide();
		}),this.timerDt);
	}
	
	///
	/// spreadClustersWithinContainer
	///
	this.spreadClustersWithinContainer = function(){
		console.log("spreadClustersWithinContainer");
		//this.resetClustersArray();
		
		/// Reset position and properties of all the clusters
		w = this.field.defaultClusterSize(this.elementsType.name).width;
		h = this.field.defaultClusterSize(this.elementsType.name).height;
		var basketX = this.basket0.style.x;
		var basketY = this.basket0.style.y;
		var basketW = this.basket0.style.width;
		var basketH = this.basket0.style.height;
		for( var i=0; i<MAX_N_DRAGGABLES; i++ ){
			var pos = {
				x: basketX + Math.random()*(basketW-w),
				y: basketY + basketH*.2 + Math.random()*(basketH*.8-h)
			}
			var c = this.field.createCluster(pos,this.elementsType);
			c.isSplittable = false;
			c.isJoinable = false;
			c.allowInertia = true;
			c.isDraggable = true;
		}
	};
	
	///
	/// detectClusterOnPlate
	///
	this.detectClusterOnPlate = function(cluster){
		var r0 = cluster.getBoundingShape();
		var p0 = r0.getCenter();
		var r1 = this.plate.getBoundingShape();
		//var intersection = intersect.rectAndRect( r0,r1 );
		var intersection = intersect.pointAndRect(p0,r1);
		return intersection;
	};
	
	///
	/// evaluateSolution
	///
	this.evaluateSolution = function(c){
		/// show cluster's label
		c.isLabelVisible = true;
		c.updateLabel();
		/// pause the game and show the solution view
		this.pause();
		solutionIsValid = c.elements.length === this.gameTargetN;
		this.solutionView.smiley.setImage( solutionIsValid ? "resources/images/icons/smiley_glasses.png" : "resources/images/icons/smiley_sad.png" );
		this.solutionView.show();
		this.solutionView.animateIn(800);
		if( solutionIsValid ){
			this.solutionView.once('InputStart',bind(this,function(){
				var t = 500;
				this.solutionView.animateOut(t);
				setTimeout(bind(this,function(){ this.reset(); this.play(); this.solutionView.hide();}),t);
				/// hide cluster's label again
				c.isLabelVisible = false;
				c.updateLabel();
			}));
		}else{
			this.solutionView.once('InputStart',bind(this,function(){
				var t = 500;
				this.solutionView.animateOut(t);
				animate(c).now({y:this.plate.style.y - c.style.height*1.5},t);
				setTimeout(bind(this,function(){ this.play(); this.solutionView.hide();}),t);
				/// hide cluster's label again
				c.isLabelVisible = false;
				c.updateLabel();
			}));
		}
	};
});

var SolutionView = Class( View, function(supr){
	this.init = function(opts){
		opts = merge(opts,{
			superview: opts.superview,
			x: 0, y: 0,
			width: GLOBAL.baseWidth,
			height: GLOBAL.baseHeight,
		});
		supr(this, 'init', [opts]);
		
		/// Semi-transparent background view
		this.bg = new View({
			superview: this,
			x:0, y:0,
			width: GLOBAL.baseWidth, height: GLOBAL.baseWidth,
			backgroundColor: '#AAAAAA',
			opacity: .5,
			//x: ( (this.style.width>=this.style.height? this.style.width : w) -w)/2,
			//y: ( (this.style.width>=this.style.height? h : this.style.width) -h)/2,
			//width: w, height: h,
		});
		
		/// Message
		var w = Math.min(this.style.width, this.style.height)*.3;
		var h = w;
		this.smiley = new ImageView({
			superview: this,
			x: w*.5,
			y: h*.5,
			width: w, height: h,
			image: "resources/images/icons/smiley_glasses.png",
			scale: 0,
			anchorX: w*.5, anchorY: h*.5,
		});
		
		/// Exit when touched
		this.on('InputStart',bind(this,function(){
			this.animateOut();
			this.emit("SolutionIsValid");
		}));
	};
	
	this.animateIn = function(t){
		animate(this.smiley)
		.now({scale:1, r:3.14*4, },t,animate.easeIn);
	};
	this.animateOut = function(t){
		animate(this.smiley)
		.now({scale:0, r:0, },t,animate.easeOut);
	};
});
