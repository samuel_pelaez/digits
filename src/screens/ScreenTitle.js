/*
 * The title screen consists of a background image and a
 * start button. When this button is pressed, and event is
 * emitted to itself, which is listened for in the top-level
 * application. When that happens, the title screen is removed,
 * and the game screen shown.
 */

import ui.View;
import ui.ImageView;
import ui.TextView;
import device;

/* The title screen is added to the scene graph when it becomes
 * a child of the main application. When this class is instantiated,
 * it adds the start button as a child.
 */
exports = Class(ui.View, function (supr) {
	this.init = function (opts) {
		opts = merge(opts, {
			x: 0,
			y: 0
		});

		supr(this, 'init', [opts]);

		this.build();
	};

	this.build = function() {
		var W = GLOBAL.baseWidth;
		var H = GLOBAL.baseHeight;
		
		/// A text with the app name: "Els digits"
		var w = W*0.6;
		var h = w*0.3;
		var titleButton = new ui.TextView({
			superview: this,
			x: (W*.8-w)*.5,
			y: (H-h)*.4,
			width: w,
			height: h,
			centerX: true,
			text: "Els digits",
			color: "red",
			size: 400,
			autoFontSize: true,
			horizontalAlign: 'center'
		});
		
		w = W*.2;
		h = w;
		var startButton = new ui.ImageView({
			superview: this,
			x: W-w*1.25,
			y: H*.1,
			width: w,
			height: h,
			image: "resources/images/icons/right_arrow.png"
		});
		
		var helpButton = new ui.ImageView({
			superview: this,
			x: W-w*1.25,
			y: H*.9-h,
			width: w,
			height: w,
			image: "resources/images/icons/help.png"
		});
		
		/* Listening for a touch or click event, and will dispatch a
		 * custom event to the title screen, which is listened for in
		 * the top-level application file.
		 */
		startButton.on('InputSelect', bind(this, function () {
			this.emit('screenTitle:start');
		}));
		helpButton.on('InputSelect', bind(this, function () {
			this.emit('screenTitle:about');
		}));
	};
});
