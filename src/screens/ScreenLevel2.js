import animate
import ui.View as View
import ui.TextView as TextView
import ui.ImageView as ImageView
import ui.ImageScaleView as ImageScaleView
import ui.resource.Image as Image
import event.input.drag as drag;
import src.utils.ClusterDefaultProperties as ClusterDefaultProperties
import src.utils.DraggPinchable as DraggPinchable
import src.utils.Reservoir as Reservoir
import src.utils.InteractionsField as InteractionsField
import src.utils.TileOfDigits as TileOfDigits
import src.utils.ClusterTypes as ClusterTypes
import math.geom.Vec2D as Vec2D
import math.geom.intersect as intersect
import ui.widget.ButtonView as ButtonView

var MAX_N_DRAGGABLES = 30;
var STATUS_PLAYING = 1, STATUS_PAUSED = 0;

exports = Class(View, function (supr) {

	this.init = function (opts) {
		var W = GLOBAL.baseWidth;
		var H = GLOBAL.baseHeight;
		opts = merge(opts, {
			x: 0,
			y: 0,
			width: W,
			height: H,
			backgroundColor:"white"
		});

		supr(this, 'init', [opts]);
		
		ClusterDefaultProperties.def.isLabelVisible = false;
		
		this.reservoir = new Reservoir();
		
		/// Interactions field
		this.field = new InteractionsField({
			superview: this,
			reservoir: this.reservoir,
			x: W*.0, y: H*.00,
			width: W, height: H*.87,
			nClusters:0,
			type: 'units',
			backgroundColor:"#5050A0",
		});
		this.field.buttonMove.hide();
		this.field.buttonResize.hide();
		this.field.buttonClear.hide();
		this.field.counter.hide();
		this.clusterTypes = new ClusterTypes();
		this.elementsType = this.clusterTypes.types['units'];
		this.elementsTypeOld = '';
		this.timerDt = 100;
		
		/// Tile of digits
		this.tileOfDigits = new TileOfDigits({
			superview: this,
			x: 0, y: H,
			width: W*.7,
		});
		
		
		this.oldGameTargetN = -1;
		this.build(opts);
		
		/// Begin to play
		animate(this).wait(100).then( bind( this,function(){
			this.reset();
			this.play();
		}));
		
		this.on("OnPlate",bind(this,function(c){this.evaluateSolution(c);}));		
	};
	
	///
	/// build
	///
	this.build = function(opts){
		this.drawGameElements();
		/// Prepare solution dialog
		this.solutionView = new SolutionView({superview: this});
		this.solutionView.hide();
		
		/// Reset button
		var W = GLOBAL.baseWidth;
		var H = GLOBAL.baseHeight;
		var w = h = H*.15;
		this.buttonReset = new ButtonView({
			superview: this,
			x: this.field.style.x + this.field.style.width - w*1.1,
			y: this.field.style.y + h*.1,
			width: w, height: h,
			images:{
				down: "resources/images/icons/refresh.png",
				up: "resources/images/icons/refresh.png",
				disabled: "resources/images/icons/refresh.png",
			},
			on:{
				up: bind(this,"reset"),
			} 
		});
		this.buttonBack = new ButtonView({
			superview: this,
			x: this.field.style.x + w*.1,
			y: this.field.style.y + h*.1,
			width: w, height: h,
			images:{
				down: "resources/images/icons/left_arrow.png",
				up: "resources/images/icons/left_arrow.png",
				disabled: "resources/images/icons/left_arrow.png",
			},
			on:{
				up: bind(this,function(){console.log("emitting exitScreen");this.emit("exitScreen");}),
			} 
		});
	};
	
	///
	/// drawGameElements
	///
	this.drawGameElements = function(){
		var W = this.field.style.width;
		var H = this.field.style.height;
		var x,y,w,h;
		
		/// Draw empty plate
		w = W*.45;	h = w*238/800.;
		x = W-w*1.2; 		y = H*.45;
		this.plateShadow = new View({
			superview: this.field,
			x: x, y: y, width: w, height: h,
			backgroundColor: "gray",
			opacity: 0.5,
			visible: false
		});
		this.plate = new ImageView({
			superview: this.field,
			x: x, y: y, width: w, height: h,
			//backgroundColor: "red", opacity: 0.5, // visual guide
			image: "resources/images/level1/plate.png"
		});
		
		/// Draw target image
		h = this.field.style.height - this.plate.style.y - this.plate.style.height;
		w = h*this.tileOfDigits.digits[1].style.width/this.tileOfDigits.digits[1].style.height;
		x = this.plate.style.x + this.plate.style.width/2. - w/2.;
		y = this.plate.style.y + this.plate.style.height;
		this.targetImage = new ImageView({
			superview: this,
			x: x, y: y,
			width: w, height: h
		});
	};
	
	///
	/// reset
	///
	this.reset = function(){
		console.log("running reset");
		do{
			this.gameTargetN = Math.floor((Math.random()*9)+1);
		}while( this.gameTargetN === this.oldGameTargetN );
		this.oldGameTargetN = this.gameTargetN;
		this.elementsType.image.setURL("resources/images/fish.png");
		this.elementsType.superType = 'none';
		this.elementsType.nMaxElements = 20;
		/// put clusters
		this.field.deleteAllClusters();
		this.spreadClustersWithinContainer();
		/// set the target image
		this.targetImage.setImage( this.tileOfDigits.digits[this.gameTargetN].getImage() );
		this.targetImage.hide();
	};
		
	///
	/// play
	///
	this.play = function(){
		console.log("running play");
		this.buttonReset.setState(ButtonView.states.UP);
		this.buttonBack.setState(ButtonView.states.UP);
		/// Resume animations, if any
		for( var i=0; i<this.field.clusters.length; i++ ){
			var c = this.field.clusters[i];
			c.setHandleEvents(true);
			animate(c).resume();
		}
		/// update status
		this.status = STATUS_PLAYING;
		/// run game timer
		this.setGameTimer();
	};
	
	///
	/// pause
	///
	this.pause = function(excludeCluster){
		console.log("running pause");
		this.buttonReset.setState(ButtonView.states.DISABLED);
		this.buttonBack.setState(ButtonView.states.DISABLED);
		/// Stop animations, if any
		for( var i=0; i<this.field.clusters.length; i++ ){
			var c = this.field.clusters[i];
			var anim = animate(c);
			if( anim.hasFrames() && c !== excludeCluster ){
				anim.pause();
			}
			c.setHandleEvents(false);
		}
		/// update status
		this.status = STATUS_PAUSED;
		/// pause game timer
		clearInterval(this.gameTimer);
	};
	
	///
	/// setGameTimer
	///
	this.setGameTimer = function(){
		/*this.gameTimer = setInterval(bind(this,function(){
			var overPlate = false;
			for(var i=0; i<this.field.clusters.length; i++ ){
				var c = this.field.clusters[i];
				if( this.detectClusterOnPlate(c) ){
					overPlate = true;
					if( !c.isDragging() && !animate(c).hasFrames() ){
						console.log("On plate");
						this.emit("OnPlate",c);
					}
				}
			}
			if( overPlate ) this.plateShadow.show();
			else this.plateShadow.hide();
		}),this.timerDt);*/
	};
	
	///
	/// spreadClustersWithinContainer
	///
	this.spreadClustersWithinContainer = function(){
		console.log("spreadClustersWithinContainer");
		//this.resetClustersArray();
		
		/// Reset position and properties of all the clusters
		w = this.field.defaultClusterSize(this.elementsType.name).width;
		h = this.field.defaultClusterSize(this.elementsType.name).height;
		var spreadBoxX = this.field.style.x*.1;
		var spreadBoxY = this.field.style.y*.1;
		var spreadBoxW = this.field.style.width*.5;
		var spreadBoxH = this.field.style.height*.5;
		for( var i=0; i<this.gameTargetN; i++ ){
			var pos = {
				x: spreadBoxX + Math.random()*(spreadBoxW-w),
				y: spreadBoxY + spreadBoxH*.2 + Math.random()*(spreadBoxH*.8-h)
			}
			var c = this.field.createCluster(pos,this.elementsType);
			c.isSplittable = false;
			c.isJoinable = true;
			c.allowInertia = true;
			c.isDraggable = true;
			c.removeListener('DragStop',this.detectClusterOnPlate);
			//c.on('DragStop',bind(this,this.detectClusterOnPlate));
			c.on('DragStop',this.detectClusterOnPlate);
		}
	};
	
	///
	/// detectClusterOnPlate
	///
	this.detectClusterOnPlate = function(dragEvt,selectEvt){
		// Notice that this function acts as a callback for the 
		// on 'DragStop' event of a cluster. For this reason, the 'this'
		// object here is the cluster itself.
		// Yes, this sounds weird, but it's how it works...
		var cluster = this; //= dragEvt.target;
		var This = cluster.getSuperview().getSuperview();
		var r0 = cluster.getBoundingShape();
		var p0 = r0.getCenter();
		var r1 = This.plate.getBoundingShape();
		//var intersection = intersect.rectAndRect( r0,r1 );
		var intersection = intersect.pointAndRect(p0,r1);
		if(intersection)
			This.evaluateSolution(cluster);
	};
	
	///
	/// evaluateSolution
	///
	this.evaluateSolution = function(c){
		console.log("evaluateSolution");
		/// animate tile of digits
		this.tileOfDigits.animateWave();
		/// show cluster's label
		c.isLabelVisible = true;
		c.updateLabel();
		/// pause the game and show the solution view
		/*this.pause();
		this.tileOfDigits.once("InputStart",bind( VOY POR AQUIIIIIII
		/*solutionIsValid = c.elements.length === this.gameTargetN;
		this.solutionView.smiley.setImage( solutionIsValid ? "resources/images/icons/smiley_glasses.png" : "resources/images/icons/smiley_sad.png" );
		this.solutionView.show();
		this.solutionView.animateIn(800);
		if( solutionIsValid ){
			this.solutionView.once('InputStart',bind(this,function(){
				var t = 500;
				this.solutionView.animateOut(t);
				setTimeout(bind(this,function(){ this.reset(); this.play(); this.solutionView.hide();}),t);
				/// hide cluster's label again
				c.isLabelVisible = false;
				c.updateLabel();
			}));
		}else{
			this.solutionView.once('InputStart',bind(this,function(){
				var t = 500;
				this.solutionView.animateOut(t);
				animate(c).now({y:this.plate.style.y - c.style.height*1.5},t);
				setTimeout(bind(this,function(){ this.play(); this.solutionView.hide();}),t);
				/// hide cluster's label again
				c.isLabelVisible = false;
				c.updateLabel();
			}));
		}*/
	};
	
});

var SolutionView = Class( View, function(supr){
	this.init = function(opts){
		opts = merge(opts,{
			superview: opts.superview,
			x: 0, y: 0,
			width: GLOBAL.baseWidth,
			height: GLOBAL.baseHeight,
		});
		supr(this, 'init', [opts]);
		
		/// Semi-transparent background view
		this.bg = new View({
			superview: this,
			x:0, y:0,
			width: GLOBAL.baseWidth, height: GLOBAL.baseWidth,
			backgroundColor: '#AAAAAA',
			opacity: .5,
			//x: ( (this.style.width>=this.style.height? this.style.width : w) -w)/2,
			//y: ( (this.style.width>=this.style.height? h : this.style.width) -h)/2,
			//width: w, height: h,
		});
		
		/// Message
		var w = Math.min(this.style.width, this.style.height)*.3;
		var h = w;
		this.smiley = new ImageView({
			superview: this,
			x: w*.5,
			y: h*.5,
			width: w, height: h,
			image: "resources/images/icons/smiley_glasses.png",
			scale: 0,
			anchorX: w*.5, anchorY: h*.5,
		});
		
		/// Exit when touched
		this.on('InputStart',bind(this,function(){
			this.animateOut();
			this.emit("SolutionIsValid");
		}));
	};
	
	this.animateIn = function(t){
		animate(this.smiley)
		.now({scale:1, r:3.14*4, },t,animate.easeIn);
	};
	this.animateOut = function(t){
		animate(this.smiley)
		.now({scale:0, r:0, },t,animate.easeOut);
	};
});

