import ui.View as View;
import ui.ImageView as ImageView;
import ui.TextView as TextView;
import device;

/// Set the current program version
var version = "0.4";

var text1 = "Els Digits.\n\n"
+"This is a preliminary version of the Els Digits software, "
+"designed as a proof of concept.\n\n"
+"Program version: "+version+"\n"
+"Date of compilation: Dec 1st, 2013";


exports = Class(View, function (supr) {
	this.init = function (opts) {
		opts = merge(opts, {
			x: 0,
			y: 0,
			backgroundColor: 'black'
		});

		supr(this, 'init', [opts]);

		this.build();
	};

	this.build = function() {
		var W = GLOBAL.baseWidth;
		var H = GLOBAL.baseHeight;
		
		/// A text with the app name: "Els digits"
		var w = W*0.85;
		var h = H*0.7;
		var titleButton = new TextView({
			superview: this,
			x: (W-w)*.5,
			y: H*0.1,
			width: w,
			height: h,
			centerX: true,
			text: text1,
			fontFamily: 'Verdana',
			color: "white",
			wrap: true,
			size: 120
		});

		/// Back button
		w = W*.1; h = w;
		var backButton = new ImageView({
			superview: this,
			x: w/4., y: h/4.,
			width: w, height:h,
			image: "resources/images/icons/left_arrow.png"
		});
		backButton.on("InputSelect", bind(this, function(){
			this.emit("exitScreen");
		}));
	};
});


