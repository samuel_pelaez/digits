import animate
import ui.View as View
import ui.TextView as TextView
import ui.ImageView as ImageView
import ui.ImageScaleView as ImageScaleView
import event.input.drag as drag;
import src.utils.DraggPinchable as DraggPinchable
import src.utils.Reservoir as Reservoir
import src.utils.InteractionsField as InteractionsField
import src.utils.TileOfDigits as TileOfDigits
import src.utils.ClusterTypes as ClusterTypes
import math.geom.Vec2D as Vec2D
import ui.widget.ButtonView as ButtonView


exports = Class(View, function (supr) {

	this.init = function (opts) {
		var W = GLOBAL.baseWidth;
		var H = GLOBAL.baseHeight;
		opts = merge(opts, {
			x: 0,
			y: 0,
			width: W,
			height: H,
			backgroundColor:"white"
		});
		this.clusterTypes = new ClusterTypes();

		supr(this, 'init', [opts]);
		
		this.reservoir = new Reservoir();
		
		/// Interactions field
		this.field = new InteractionsField({
			superview: this,
			reservoir: this.reservoir,
			x: W*.0, y: H*.00,
			width: W, height: H*.87,
			nClusters:0,
			type: 'units',
			backgroundColor:"#50A050",
		});
		this.field.buttonMove.hide();
		this.field.buttonResize.hide();
		
		/// Tile of digits
		this.tileOfDigits = new TileOfDigits({
			superview: this,
			x: 0, y: H,
			width: W*.7,
		});
		
		/// Red frame for digits
		this.digitSelector = new ImageScaleView({
			superview: this.tileOfDigits,
			x: this.tileOfDigits.digits[1].style.x,
			y: this.tileOfDigits.digits[1].style.y,
			width: this.tileOfDigits.digits[1].style.width,
			height: this.tileOfDigits.digits[1].style.height,
			image: "resources/images/frame-simple-red.png", 
			scaleMethod: '9slice',
			sourceSlices: {
				horizontal:{left:10,center:44,right:10},
				vertical:{top:10,middle:44,bottom:10}},
			destSlices: {
				horizontal:{left:5,right:5},
				vertical:{top:5,bottom:5}},
		});
		
		/// Back button (to exit this screen)
		var w = h = H*.15;
		this.buttonBack = new ButtonView({
			superview: this,
			x: this.field.style.x + w*.1,
			y: this.field.style.y + h*.1,
			width: w, height: h,
			images:{
				down: "resources/images/icons/left_arrow.png",
				up: "resources/images/icons/left_arrow.png",
				disabled: "resources/images/icons/left_arrow.png",
			},
			on:{
				up: bind(this,function(){console.log("emitting exitScreen");this.emit("exitScreen");}),
			} 
		});
		
		
		
		this.build(opts);

		/// Catch digits touch
		this.selectedDigit = 1;
		this.tileOfDigits.on("digitTouched", bind(this,function(i){
			this.selectDigit(i);
		}));
		
		
	};
	
	this.build = function(opts){
		/// Source
		var w = this.style.width*.3;
		var h = w/4;
		this.source = new SourceBox({
			superview: this,
			x: this.style.width - w*1.02,
			y: this.style.height - h*1.02,
			width: w, height: h,
		});
		this.source.on("SourceSelected",bind(this,function(i){
			var p1 = new Vec2D(this.source.getPosition(this)).multiply(1/this.getPosition().scale);
			var p2 = new Vec2D(this.field.getPosition(this)).multiply(1/this.getPosition().scale);
			var pos = p1.minus(p2).minus({x:100,y:100});
			var typeName = i===1? 'units'
					: i=== 10? 'tens'
					: i=== 100? 'hundreds'
					: i=== 1000? 'thousands'
					: undefined;
			var type = this.clusterTypes.types[typeName];
			var N = this.selectedDigit;
			if( N === 0 ){ 
				this.selectDigit(1);
				return; 
			}
			var c1 = this.field.createCluster(pos,type,N);
			c1.isJoinable = false;
			c1.style.scale = 0.2;
			c1.style.opacity = 0.2;
			/// Move towards the center of the field
			var t = 200;
			animate(c1).now({
				x: (this.field.style.width-c1.style.width)/2.,
				y: (this.field.style.height-c1.style.height)/2,
				scale: 1, opacity: 1
			},t,animate.easeOut);
			setTimeout( function(){
				c1.isJoinable = true;
			},t*1.3);
			/// Reset the selected digit
			this.selectDigit(1);
		}));
	};
	
	this.selectDigit = function(i){
		this.selectedDigit = i;
		animate(this.digitSelector).now({
			x: this.tileOfDigits.digits[i].style.x,
			y: this.tileOfDigits.digits[i].style.y,
		},300);
	};
	
	
});

///
/// SourceBox
///
var SourceBox = Class(View, function(supr){
	this.init = function (opts) {
		opts = merge(opts, {
			x: 0,
			y: 0,
			width: W*.1,
			height: W*.1/4,
			field: opts.superview
		});

		supr(this, 'init', [opts]);
		
		this.field = opts.field;
		
		this.img = [
			"resources/images/cubes/cubes-one.png",
			"resources/images/cubes/cubes-ten.png",
			"resources/images/cubes/cubes-hundred.png",
			"resources/images/cubes/cubes-thousand.png",
		];
		var N = this.img.length;
		var w = this.style.width/N;
		var h = w;
		this.images = [];
		this.box = [];
		for( var i=0; i<N; i++ ){
			/// The image of the cluster
			this.images.push( new ImageView({
				superview: this,
				width: w,
				height: h,
				image: this.img[i],
				scale: 0.8,
			}));
			/// A frame
			this.box.push( new ImageScaleView({
				superview: this,
				x: w*i, y: 0,
				width: w,
				height: h,
				image: "resources/images/frame-simple-black.png", 
				scaleMethod: '9slice',
				sourceSlices: {
					horizontal:{left:10,center:44,right:10},
					vertical:{top:10,middle:44,bottom:10}},
				destSlices: {
					horizontal:{left:5,right:5},
					vertical:{top:5,bottom:5}},
			}));
		}
		this.images[0].style.update({width: w/2., height: w/2.});
		this.images[1].style.update({height: w/10.});
		this.images[2].style.update({height: w*.3});
		for( var i=0; i<N; i++ ){
			var s = this.images[i].style.copy();
			var x = (this.box[i].style.width-s.width*s.scale)/2.;
			var y = (this.box[i].style.height-s.height*s.scale)/2.;
			x += this.box[i].style.x;
			this.images[i].style.update({x:x, y:y});
			
			/// Label
			var label = new TextView({
				superview: this.box[i],
				x: w*.3,
				y: w*.6,
				width: w*.4, height: w*.4,
				backgroundColor: 'gray',
				text: i.toString(),
				scale: 0.8,
				zIndex: this.style.zIndex + 1,
				opacity: 1,
			});
			if( i === 0 ) label.setText('1');
			if( i === 1 ) label.setText('10');
			if( i === 2 ) label.setText('100');
			if( i === 3 ) label.setText('1000');
			
			/// Emit a signal when selected
			this.box[i].on('InputStart',bind(this.box[i],function(evt,pt){
				var j = this.style.x/this.style.width;
				var n = j===0? 1 
						: j===1? 10 
						: j===2? 100 
						: j===3? 1000 
						: 0;
				this.getSuperview()
					.emit('SourceSelected', n);
			}));
		}
	};
});











