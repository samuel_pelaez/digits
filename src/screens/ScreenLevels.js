/*
 * The title screen consists of a background image and a
 * start button. When this button is pressed, and event is
 * emitted to itself, which is listened for in the top-level
 * application. When that happens, the title screen is removed,
 * and the game screen shown.
 */

import ui.View;
import ui.ImageView;
import ui.TextView;
import ui.widget.GridView;
import device;

/* The title screen is added to the scene graph when it becomes
 * a child of the main application. When this class is instantiated,
 * it adds the start button as a child.
 */
exports = Class(ui.View, function (supr) {
	this.init = function (opts) {
		opts = merge(opts, {
			x: 0,
			y: 0
		});

		supr(this, 'init', [opts]);

		this.build();
	};

	this.build = function() {
		var W = GLOBAL.baseWidth;
		var H = GLOBAL.baseHeight;

		var w = W*.85;
		var h = H*.95;
		// Determine the dimensions of horizontal and vertical margins.
		// This is important to guarantee that images within the grid keep 
		// their aspect ratio.
		var ncols = 3; var nrows = 3;
		var wc = w/ncols; var hc = h/nrows;  // width and height of a cell
		var ar = 1;  // aspect ratio (width/height) of the images that will be drawn within the cells
		var margin_min = 0; // minimum margin 
		var mx = 0; var my = 0; // horizontal and vertical margins
		if( W>H ){
			my = margin_min;
			mx = ar*wc + 2*my - hc;
		}
		else{
			mx = margin_min;
			my = hc + 2*ar*mx - ar*wc;
		}
		
		// Set the grid dimensions
		var grid = new ui.widget.GridView({
			cols: ncols, rows: nrows,
			backgroundColor: "#e0d6bf",
			superview: this,
			x: (W-w),
			y: H*.01,
			width: w,
			height: h,
			horizontalMargin: mx/2.,
			verticalMargin: my/2.
		});
		
		var level1 = new ui.ImageView({
			superview: grid,
			col: 0, row: 0,
			image: "resources/images/xifres/1j.png"
		});
		
		var level2 = new ui.ImageView({
			superview: grid,
			col: 1, row: 0,
			image: "resources/images/xifres/2j.png"
		});

		var level3 = new ui.ImageView({
			superview: grid,
			col: 2, row: 0,
			image: "resources/images/xifres/3j.png"
			,opacity: 0.6
		});

		var level4 = new ui.ImageView({
			superview: grid,
			col: 0, row: 1,
			image: "resources/images/xifres/4j.png"
			,opacity: 0.6
		});

		var level5 = new ui.ImageView({
			superview: grid,
			col: 1, row: 1,
			image: "resources/images/xifres/5j.png"
			,opacity: 0.6
		});

		var level6 = new ui.ImageView({
			superview: grid,
			col: 2, row: 1,
			image: "resources/images/xifres/6j.png"
			,opacity: 0.6
		});

		var level7 = new ui.ImageView({
			superview: grid,
			col: 0, row: 2,
			image: "resources/images/xifres/7j.png"
			,opacity: 0.6
		});

		var level8 = new ui.ImageView({
			superview: grid,
			col: 1, row: 2,
			image: "resources/images/xifres/8j.png"
			,opacity: 0.6
		});
		
		var level9 = new ui.ImageView({
			superview: grid,
			col: 2, row: 2,
			image: "resources/images/lab.png"
		});
		
		
		/// Back button
		w = W*.1; h = w;
		x = w/4.; y = h/4.;
		this.backButton = new ui.ImageView({
			superview: this,
			x: x, y: y,
			width: w, height:h,
			image: "resources/images/icons/left_arrow.png"
		});
		this.backButton.on("InputSelect", bind(this, function(){
			this.emit("exitScreen");
		}));
		


		level1.on('InputSelect', bind(this, function () {
			this.emit('screenLevels:level1');
		}));
		level2.on('InputSelect', bind(this, function () {
			this.emit('screenLevels:level2');
		}));
		level9.on('InputSelect', bind(this, function () {
			this.emit('screenLevels:levelInteractionsField');
		}));
		
	};
	
});
