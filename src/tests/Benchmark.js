import ui.View as View;
import src.utils.Reservoir as Reservoir
import src.utils.InteractionsField as InteractionsField


exports = Class(View, function (supr) {

	this.init = function (opts) {
		var W = GLOBAL.baseWidth;
		var H = GLOBAL.baseHeight;
		opts = merge(opts, {
			x: 0,
			y: 0,
			backgroundColor:"white"
		});

		supr(this, 'init', [opts]);
		
		this.reservoir = new Reservoir();
		
		this.field = new InteractionsField({
			superview: this,
			reservoir: this.reservoir,
			x: W*.05, y: H*.05,
			width: W*.9, height: H*.9,
			nClusters:10,
			type: 'units'
		});

		this.build();

	};
	
	this.build = function(){
	};
	
	
});












