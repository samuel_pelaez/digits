import ui.StackView as StackView;
import device;

GLOBAL.W = device.width;
GLOBAL.H = device.height;
GLOBAL.BOUNDS_WIDTH = 576;
GLOBAL.BOUNDS_HEIGHT = 1024;

import src.screens.ScreenTitle as ScreenTitle
import src.screens.ScreenLevels as ScreenLevels
import src.screens.ScreenAbout as ScreenAbout
import src.screens.ScreenInteractionsField as ScreenInteractionsField
import src.screens.ScreenLevel1 as ScreenLevel1
import src.screens.ScreenLevel2 as ScreenLevel2
import src.tests.Benchmark as Benchmark	// TODO: remove


exports = Class(GC.Application, function () 
{
	this.initUI = function () {
		this.updateScreenDimensions('landscape');
		var screenTitle = new ScreenTitle();
		var screenAbout = new ScreenAbout();
		var screenLevels = new ScreenLevels();
		var benchmark = new Benchmark();
		var screenInteractionsField = new ScreenInteractionsField();
		var screenScene1 = new ScreenLevel1();
		var screenScene2 = new ScreenLevel2();
		var rootView = new StackView({
			superview: this,
			x: 0,
			y: 0,
			width: GLOBAL.baseWidth,
			height: GLOBAL.baseHeight,
			backgroundColor: 'black',
			scale: GLOBAL.scale
		});
		rootView.push(screenTitle);
		//rootView.push(benchmark);
		//rootView.push(screenInteractionsField);
		//rootView.push(screenScene1);
		//rootView.push(screenScene2);
		
		/// Interactions
		///
		///		Title screen
		screenTitle.on('screenTitle:start', function () {
			rootView.push(screenLevels);
		});		
		screenTitle.on('screenTitle:about', function () {
			rootView.push(screenAbout,true);
		});
		///
		///		About screen
		screenAbout.on('exitScreen', function () {
			rootView.pop();
		});
		///
		///		Levels screen
		screenLevels.on('screenLevels:level1', function () {
			rootView.push(screenScene1);
		});
		screenLevels.on('screenLevels:level2', function () {
			rootView.push(screenScene2);
		});
		screenLevels.on('screenLevels:levelInteractionsField', function () {
			rootView.push(screenInteractionsField);
		});
		screenLevels.on('exitScreen', function () {
			rootView.pop();
		});
		///
		///		Scene 1
		screenScene1.on('exitScreen', function () {
			rootView.pop();
		});
		///
		///		Scene 2
		screenScene2.on('exitScreen', function () {
			rootView.pop();
		});
		///
		///		ScreenInteractionsField
		screenInteractionsField.on('exitScreen', function () {
			rootView.pop();
		});
		
		/// Back button
		if( device.isAndroid ){
			device.setBackButtonHandler(function() {
				rootView.getCurrentView().emit('exitScreen');
			});
		}
	};
	
	///
	/// updateScreenDimensions
	///
	/// Handle universal scaling, suitable for all devices.
	this.updateScreenDimensions = function(orientation){
		if(orientation==='portrait'){
			GLOBAL.baseWidth = GLOBAL.BOUNDS_WIDTH; //576
			GLOBAL.baseHeight =  device.screen.height * (GLOBAL.BOUNDS_WIDTH / device.screen.width); //864
			GLOBAL.scale = device.screen.width / baseWidth; //1
		}else if(orientation==='landscape'){
			GLOBAL.baseWidth = device.screen.width * (GLOBAL.BOUNDS_HEIGHT / device.screen.height); //864
			GLOBAL.baseHeight = GLOBAL.BOUNDS_HEIGHT; //576
			GLOBAL.scale = device.screen.height / baseHeight; //1
		}else{
			console.log("Error! Invalid orientation in " +
						"updateScreenDimensions: "+orientation);
		}
	};
});
