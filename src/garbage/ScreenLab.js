import ui.View as View
import ui.ImageView as ImageView
import device
import ui.resource.Image as Image
import AudioManager;

import math.geom.Point as Point
import math.geom.Rect as Rect
import math.geom.intersect as intersect;
import math.util as util;
import animate;

import src.utils.MyUtils as MyUtils
import src.utils.Cluster as Cluster
import src.utils.Animations as Animations
import src.utils.TileOfDigits as TileOfDigits


exports = Class( ImageView, function(supr) 
{
	this.init = function (opts) {
		opts = merge(opts, {
			x: 0, y: 0,
			width:	device.width,
			height: device.height,
			backgroundColor: "#DDDDDD",
			image: "resources/images/grass.png"
		});
		
		supr(this, 'init', [opts]);

		this.build(opts);
	};
	
	this.build = function(opts)
	{
		var W = opts.width;
		var H = opts.height;

		this.blocks = [];
		this.clusters = [];
		
		this.sound = new AudioManager({
			path: "resources/audio/",
			files: {
				birds: {volume: 0.9},
				jsbach: {volume: 0.9, background: true},
				chord: {volume: 0.9},
				rattle: {volume: 0.9},
				glass: {volume: 0.9},
				smash: {volume: 0.9}
			}
		});
		
		/// Draw all digits
		/*this.tileOfDigits = new TileOfDigits({
			superview: this,
			x: 0, y: device.height,
			width: device.width
		});
		for( var n=2; n<=9; n++ ){
			this.tileOfDigits.on('digitTouched', bind(this,function(n){
				this.resetObjects(n);
			}));
		}*/
		w = W/11.;
		h = w*90/113.;
		this.digits = [];
		for( var i=0; i<10; i++ )
		{
			this.digits.push( new ImageView({
				superview: this,
				x: (i+.5)*w,
				y: H-h,
				width: w,
				height: h,
				image: "resources/images/xifres/"+i+"j.png"
			}));
		}
		this.digits[2].on('InputStart', bind(this,function(ev,pt){
			this.resetObjects(2);
			Animations.dance(this.digits[2]);
		}));
		this.digits[3].on('InputStart', bind(this,function(ev,pt){
			this.resetObjects(3);
			Animations.dance(this.digits[3]);
		}));
		this.digits[4].on('InputStart', bind(this,function(ev,pt){
			this.resetObjects(4);
			Animations.dance(this.digits[4]);
		}));
		this.digits[5].on('InputStart', bind(this,function(ev,pt){
			this.resetObjects(5);
			Animations.dance(this.digits[5]);
		}));
		this.digits[6].on('InputStart', bind(this,function(ev,pt){
			this.resetObjects(6);
			Animations.dance(this.digits[6]);
		}));
		this.digits[7].on('InputStart', bind(this,function(ev,pt){
			this.resetObjects(7);
			Animations.dance(this.digits[7]);
		}));
		this.digits[8].on('InputStart', bind(this,function(ev,pt){
			this.resetObjects(8);
			Animations.dance(this.digits[8]);
		}));
		this.digits[9].on('InputStart', bind(this,function(ev,pt){
			this.resetObjects(9);
			Animations.dance(this.digits[9]);
		}));
		

		/// Back button
		w = W*.1; h = w;
		x = w/4.; y = h/4.;
		this.backButton = new ImageView({
			superview: this,
			x: x, y: y,
			width: w, height:h,
			image: "resources/images/icons/left_arrow.png"
		});
		this.backButton.on("InputSelect", bind(this, function(){
			this.emit("exitScreen");
		}));
		
		this.digitsWannaDemandAttention = true;

	};
	
	///
	/// digitsDemandAttention
	///
	/// Randomly, one digit dances, calling the user's attention.
	this.digitsDemandAttention = function(){
		if( !this.digitsWannaDemandAttention ){ return; }
		var tmp = new View();
		animate(tmp).wait(util.random(2000,4000)).then(function(){
			//this.tileOfDigits.animateWave();
			if( !this.digitsWannaDemandAttention ){ return; }
			var i = util.random(2,10);
			Animations.dance(this.digits[i]);
			this.sound.play('birds');
			this.digitsDemandAttention();
		}.bind(this));
	};
	
	///
	/// resetObjects
	///
	/// Spread objects randomly through the screen
	this.resetObjects = function( nBlocks ){
		this.digitsWannaDemandAttention = false;
		this.sound.play('rattle');
		/// Screen dimensions
		var W = device.width;
		var H = device.height;

		/// If there were old blocks or clusters, delete them
		for( var i=this.blocks.length-1; i>=0; i-- ){
			this.blocks[i].removeFromSuperview();
		}
		for( var i=this.clusters.length-1; i>=0; i-- ){
			this.clusters[i].removeFromSuperview();
		}
		this.blocks = [];
		this.clusters = [];

		/// Draw draggable blocks (apples)
		w = W<H ? W*.1 : H*.1;
		h = w;
		var W_ = W*.8, H_ = H-W/11.*2.;
		this.spreadArea = new Rect(w/2., h*1, W_, H_);
		for( var i=0; i<nBlocks; i++ ){
			/// Generate random coordinates for each new block
			var newX, newY;
			var overlapping = false;
			var nTries = 0;
			do{
				overlapping = false;
				newX = util.random(0,W_-w) + this.spreadArea.x;
				newY = util.random(0,H_-h) + this.spreadArea.y;
				/// Check that this new coordinate is far away enough from any
				/// cluster already created.
				for( var j=0; j<i; j++ ){
					var p0 = new Point(newX,newY);
					var p1 = new Point(this.blocks[j].style.x,
										this.blocks[j].style.y);
					if( p1.subtract(p0).getMagnitude() < 2.5*w ){
						overlapping = true;
						break;
					};
				};
				nTries++;
				if( nTries > 10 ){
					console.log("Can't avoid overlapping");
					this.resetObjects(nBlocks);
					return false;
				}
			}
			while( overlapping );
			/// Actually create the new block
			this.blocks.push( new ImageView({
				superview: this,
				image: "resources/images/apple.png",
				width: w,
				height: h,
				x:util.random(newX),
				y:util.random(newY)
			}));
		}
		
		for( var i=0; i<this.blocks.length; i++ ){	
			/// Create a cluster for each block
			this.clusters.push( new Cluster({
				superview:this,
				views:[this.blocks[i]],
				isSizeLableVisible: false
			}));
		}
		
		/// Make the clusters (and hence, apples) fall from above
		var t = 1000;
		for( var i=0; i<this.clusters.length; i++ ){
			this.clusters[i].style.y -= H;
			this.clusters[i].enteringAnimation = true;
			animate(this.clusters[i]).
				wait(Math.random()*t).
				then({
					y: this.clusters[i].style.y + H
				}, t, animate.easeIn).
				then(function(){
					this.sound.play('smash');
					this.enteringAnimation = false;
				}.bind(this.clusters[i]));
		}
		
		for( var i=0; i<this.clusters.length; i++ ){
			var obj = this.clusters[i];
			animate(this.clusters[i]).wait(t*0).then(function(){
				this.updateLabel();
			}.bind(obj));
			this.resetEventListening(obj);
		}
	}
	
	this.resetEventListening = function(clusterI){
		/// Check for clusters collisions and splits
		clusterI.removeAllListeners('Moving');
		clusterI.removeAllListeners('SplitRequired');
		clusterI.on('Moving', bind(clusterI, function () {
			var sup = this.getSuperview();
			if( typeof sup !== 'undefined' )
			{
				sup.handleCollisions( this, sup.clusters );
				sup.handleWalls( this );
			};
		}));
		clusterI.on('SplitRequired', bind(clusterI, function() {
			var sup = this.getSuperview();
			if( typeof sup !== 'undefined' && this.isSplittable ){
				sup.splitCluster( this );
			}
		}));
	};
	
	/// When a cluster touches the border of the "spread area", it
	/// no longer has inertia.
	this.handleWalls = function( cluster ){
		var A = this.spreadArea;
		var rect = new Rect( A.x, A.y, 
							A.width-cluster.style.width, 
							A.height-cluster.style.height );
		if( cluster.style.x < rect.x 
					|| cluster.style.y < rect.y
					|| cluster.style.x > rect.x+rect.width
					|| cluster.style.y > rect.y+rect.height ){
			animate(cluster).clear();
		}
	};
	
	///
	/// printViews
	///
	this.printViews = function(){
		var msg = "views: \n";
		for( var i=0; i<this.clusters.length; i++ ){
			var v = this.clusters[i];
			msg += v+"("+v.views.length+" views)   ("+~~(v.style.x)+","+~~(v.style.y)+") "+~~(v.style.width)+"x"+~~(v.style.height)+"\tisDraggable: "+v.isDraggable+"\n";
		}
		console.log(msg+"\n");
	};
	
	///
	/// splitCluster
	///
	this.splitCluster = function( cluster/*, evt*/ ){
		if( ! cluster.isSplittable ) return;
		var touchOrig = cluster.touchPoint[0].x;
		var touchNew = cluster.touchPoint[1].x;
		if( touchNew < touchOrig ){ /// new touch is left from original
			var I = Math.floor(touchNew/cluster.views[0].style.width)+1;
			var side = 'left';
		}
		else{
			var I = Math.floor(touchNew/cluster.views[0].style.width);
			var side = 'right';
		}
		/// Resize cluster
		var N = cluster.views.length - I;
		if( side === 'right' ){
			console.log("Ready to split cluster "+cluster+" in "+N+" and "+I);
			var removedViews = cluster.views.slice(I);
			cluster.popNElements( N, side );
		}else{
			console.log("Ready to split cluster "+cluster+" in "+I+" and "+N);
			var removedViews = cluster.views.slice(0,I);
			cluster.popNElements( I, side );
		}
		this.sound.play('glass');
		/// Make the cluster non joinable for a while
		cluster.isJoinable = false;
		/// Create new cluster
		this.clusters.push( new Cluster( {
			superview:this,
			views: removedViews,
			isSizeLableVisible: true,
			isSplittable: false,
			isJoinable: false
		}) );
		/// Update the dragOffset of the original cluster if spliced by the left.
		if( side === 'left' ){
			cluster.dragOffset.x -= I*cluster.views[0].style.width;
		}
		var newCluster = this.clusters[this.clusters.length-1];
		/// Let the new cluster listen form 'Moving' and 'SplitRequired'
		this.resetEventListening(newCluster);
		/// Separate clusters
		var w = cluster.views[0].style.width/4.;
		if( side === 'left' ){
			cluster.style.x += w;
			newCluster.style.x -= w;
		}else{
			cluster.style.x -= w;
			newCluster.style.x += w;
		}
	};
	
	///
	/// handleCollisions
	///
	/// Runs through all the objects in a given cluster list and check whether
	/// there is a collision between them and a given object. I such a collision
	/// exists, objects are joined together
	this.handleCollisions = function( cluster, clustersList ){
		/// Check whether 'cluster' collides with the rest of clusters
		var colliders = this.detectClusterCollision( cluster, clustersList).ret;
		if( !colliders.length ){
			/// Check if 'cluster' has splitted recently and is now stopped
			/// overlapping with its neighbors. If that's the case, make it
			/// joinable and splittable again.
			if( !cluster.isJoinable ){	
				cluster.isJoinable = true;
				cluster.isSplittable = true;
			}
			return;
		}
		/// If there are collisions, but cluster is not joinable, return.
		if( !cluster.isJoinable ){
			return;
		}
		this.sound.play('chord');
		colliders[0].isDraggable = false;
		if( cluster.style.x + .5*cluster.style.width <
				colliders[0].style.x + .5*colliders[0].style.width ){
			cluster.addViews(colliders[0].views);
		}else{
			cluster.addViews(colliders[0].views,'left');
		}
		//animate(cluster).clear();
		/// delete cluster
		var idx = this.clusters.indexOf(colliders[0]);
		this.clusters.splice(idx,1);
		colliders[0].removeFromSuperview();
		/// if there is only one cluster left, make digits demmand attention 
		/// again
		if( this.clusters.length == 1 ){
			this.digitsWannaDemandAttention = true;
			this.digitsDemandAttention();
		}
	};
	
	///
	/// detectClusterCollision
	///
	/// Runs through all the objects in container and check which collides
	/// with the given object. 
	/// Returns a list with the objects that collide with the given object.
	this.detectClusterCollision = function( obj, container ){
		var colliders = [];
		if( container.length == 0 || !obj.isDraggable )
		{
			return {ret:colliders};
		}
		var r0,r1;
		r0 = obj.getBoundingShape();
		for( var i = 0; i<container.length; i++ )
		{
			r1 = container[i].getBoundingShape();
			if( obj != container[i] && intersect.rectAndRect(r0,r1) )
			{
				console.log("detected intersect between "+obj+" and "
							+container[i]);
				colliders.push( container[i] );
			}
		}
		return {ret:colliders};
	}
	
});
















