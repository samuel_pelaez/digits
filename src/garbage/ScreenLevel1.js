import ui.ImageView as ImageView
import ui.View as View
import ui.SpriteView as SpriteView
import ui.ViewPool as ViewPool
import math.util as util
import math.geom.Rect as Rect
import math.geom.intersect as intersect;
import src.utils.TileOfDigits as TileOfDigits
import src.utils.Cluster as Cluster
import src.utils.MyUtils as MyUtils;
import device
import animate
import AudioManager
import menus.constants.menuConstants as menuConstants;
import menus.views.TextDialogView as TextDialogView;
import menus.views.DocumentView as DocumentView;


var W, H;
var w,h,x,y;
W = device.width;
H = device.height;
var MAX_N_DRAGGABLES = 30;
var STATUS_PLAYING = 1, STATUS_PAUSED = 0;
exports = Class( ImageView, function(supr) {
	this.init = function (opts) {
		opts = merge(opts, {
			x: 0, y: 0,
			width:	W,
			height: H,
			backgroundColor: "#cbe0bf",
			//image: "resources/images/grass.png"
		});
		
		supr(this, 'init', [opts]);
		
		/// Needed by GC's menus
		this.baseWidth = device.width*.9;
		this.baseHeight = device.height*.9;
		this.scale = this.style.scale;
		
		this.oldGameTargetN = -1;
		this.oldItemsImage = "";

		this.build(opts);
	};
		
	///
	/// createTextDialog
	///
	this.createTextDialog = function (title,text) {
		this._textDialogMenu = new TextDialogView({
			superview: this,
			title: title,
			text: text,
			closeCB: bind(this,function(){})
			//,width: W*.8
			//,height: H*.8
		});
	};

	///
	/// build
	///
	this.build = function(opts){
		/// Tile of digits
		this.tileOfDigits = new TileOfDigits({
			superview: this,
			x: 0, y: device.height,
			width: device.width
		});
		
		/// Back button
		w = W*.1; h = w;
		x = w/4.; y = h/4.;
		this.backButton = new ImageView({
			superview: this,
			x: x, y: y,
			width: w, height:h,
			image: "resources/images/icons/left_arrow.png"
		});
		this.backButton.on("InputSelect", bind(this, function(){
			this.emit("exitScreen");
		}));
		
		/// Help button
		w = W*.1; h = w;
		x = this.backButton.style.x + this.backButton.style.width + w/4.;
		y = h/4.;
		this.helpButton = new ImageView({
			superview: this,
			x: x, y: y,
			width: w, height:h,
			image: "resources/images/icons/help.png"
		});
		this.helpButton.on("InputSelect", bind(this, function(){
			var help = "Take elements from the basket and form a group "
						+ "of the given size. Then put the group over the "
						+ "plate.\nGood luck!!\n(This text must be replaced ";
						+ "by a set if illustrations)";
			this.createTextDialog('Help',help);
			this._textDialogMenu.show();
		}));
		
		/// Refresh button
		w = W*.1; h = w;
		x = W - w*5/4.;
		y = h/4.;
		this.refreshButton = new ImageView({
			superview: this,
			x: x, y: y,
			width: w, height:h,
			image: "resources/images/icons/refresh.png"
		});
		this.refreshButton.on("InputSelect", bind(this, function(){
			this._confirmRestartDialog = new TextDialogView({
				superview: this,
				title: 'Restart',
				text: 'Do you want to restart this level?',
				height: device.width > device.height ? 350 : 450,
				modal: true,
				buttons: [
					{
						title: 'Cancel',
						width: 200,
						style: 'RED'
					},
					{
						title: 'Restart',
						width: 200,
						style: 'GREEN',
						cb: bind(this,'play')
					}
				]
			}).show();
		}));
		
		/// A ViewPool for clusters
		w = W*.08; h = w;
		this.clustersPool = new ViewPool({
			ctor: Cluster,
			initcount: MAX_N_DRAGGABLES,
			initOpts: {
				superview: this,
				width: w,
				height: h,
				views: [],
				isSizeLableVisible: false,
				isSplittable: false,
				isJoinable: false
			}
		});
		
		/// A ViewPool for clusters' images
		this.imagesPool = new ViewPool({
			ctor: ImageView,
			initCount: MAX_N_DRAGGABLES,
			initOpts: {
				x: 0, y: 0,
				width: w, height: h,
				image: this.itemsImage()
			}
		});
		
		/// Sounds
		this.sound = new AudioManager({
			path: "resources/audio/",
			files: {
				birds: {volume: 0.9},
				jsbach: {volume: 0.9, background: true},
				chord: {volume: 0.9},
				rattle: {volume: 0.9},
				glass: {volume: 0.9},
				smash: {volume: 0.9}
			}
		});
			
		/// Draw other elements
		this.randomSeed = 0;
		this.drawGameElements();
	};
	
	///
	/// drawGameElements
	///
	this.drawGameElements = function(){
		
		/// Draw basket's background
		w = W*20/100.; h = w*1.1;
		x = W*.05; y = H*.5;
		this.basket0 = new ImageView({
			superview: this,
			x: x, y: y, width: w, height: h,
			//backgroundColor: "red", opacity: 0.5, // visual guide
			image: "resources/images/level1/basket0.png"
		});
		
		/// Draw basket's foreground, copying style from basket's background
		w = this.basket0.style.width;	h = this.basket0.style.height;
		x = this.basket0.style.x; 		y = this.basket0.style.y;
		this.basket1 = new ImageView({
			superview: this,
			x: x, y: y, width: w, height: h,
			//backgroundColor: "red", opacity: 0.5, // visual guide
			image: "resources/images/level1/basket1.png",
			zIndex: 1
		});
		this.basket1.setHandleEvents(false);
		
		/// Draw empty plate
		w = W*.45;	h = w*238/800.;
		x = W-w*1.2; 		y = H*.45;
		this.plateShadow = new View({
			superview: this,
			x: x, y: y, width: w, height: h,
			backgroundColor: "gray",
			opacity: 0.5,
			visible: false
		});
		this.plate = new ImageView({
			superview: this,
			x: x, y: y, width: w, height: h,
			//backgroundColor: "red", opacity: 0.5, // visual guide
			image: "resources/images/level1/plate.png"
		});
		
		/// Draw target image
		w = W*.2; h = w*.9;
		x = this.plate.style.x + this.plate.style.width/2. - w/2.;
		y = this.plate.style.y + this.plate.style.height*.90;
		this.targetImage = new ImageView({
			superview: this,
			x: x, y: y,
			width: w, height: h
		});
		
		/// Prepare solution dialog
		this.createSolutionDialog();
		
		/// Begin to play
		animate(this).wait(100).then( bind( this,function(){
			this.play();
		}));
	};
	
	///
	/// play
	///
	this.play = function(){
		console.log("running play");
		do{
			this.gameTargetN = util.random(1,10);
		}while( this.gameTargetN === this.oldGameTargetN );
		this.oldGameTargetN = this.gameTargetN;
		this.spreadClustersWithinContainer();
		this.targetImage.setImage(
				"resources/images/xifres/"+this.gameTargetN+"j.png");
		this.status = STATUS_PLAYING;
	};
	
	///
	/// pause
	///
	this.pause = function(excludeCluster){
		for( var i=0; i<this.clusters.length; i++ ){
			var c = this.clusters[i];
			var anim = animate(c);
			if( anim.hasFrames() && c !== excludeCluster ){
				anim.pause();
			}
			c.setHandleEvents(false);
		}
		this.status = STATUS_PAUSED;
	};
	
	///
	/// resume
	///
	this.resume = function(){
		for( var i=0; i<this.clusters.length; i++ ){
			var c = this.clusters[i];
			c.setHandleEvents(true);
			animate(c).resume();
		}
		this.status = STATUS_PLAYING;
	};
	
	///
	/// spreadClustersWithinContainer
	///
	this.spreadClustersWithinContainer = function(){
		console.log("spreadClustersWithinContainer");
		this.resetClustersArray();
		
		/// Reset position and properties of all the clusters
		w = this.clusters[0].style.width;
		h = this.clusters[0].style.height;
		for( var i=0; i<this.clusters.length; i++ ){
			var c = this.clusters[i];
			c.isSplittable = false;
			c.isJoinable = false;
			c.allowInertia = true;
			c.isDraggable = true;
			animate(c).clear();
			x = this.basket0.style.x 
					+ Math.random()*(this.basket0.style.width-w);
			y = this.basket0.style.y + this.basket0.style.height*.2
					+ Math.random()*(this.basket0.style.height*.8-h);
			c.style.x = x;
			c.style.y = y;
			c.style.scale = 1;
			c.setHandleEvents(true);
		}
	};
	
	///
	/// resetClustersArray
	///
	this.resetClustersArray = function(){
		/// Clean up all
		MyUtils.cleanPoolAndArray(this.imagesPool,this.images);
		MyUtils.cleanPoolAndArray(this.clustersPool,this.clusters);
		this.clusters = [];
		
		/// Create clusters and their images
		w = W/10.; h = w;
		var imagePath;
		do{
			imagePath = this.itemsImage();
		}while(imagePath === this.oldItemsImage);
		this.oldItemsImage = imagePath;
		for( var i=0; i<MAX_N_DRAGGABLES; i++ ){
			var newCluster = this.clustersPool.obtainView();
			var newElement = this.imagesPool.obtainView();
			newElement.updateOpts({
				visible:true, 
				superview: this.clusters[i]
			});
			newCluster.isDraggable = true;
			newCluster.isPinchable = true;
			newCluster.allowInertia = true;
			newElement.setImage(imagePath);
			newCluster.addView( newElement );
			this.clusters.push( newCluster );
			newCluster.hideLabel();
			newCluster.updateOpts({ visible: true });
			/// Listen for events on the new cluster
			this.resetEventListening(newCluster);
		};
	};
	
	///
	/// itemsImage
	///
	/// The image to use for draggable items (apples, candies, etc)
	this.itemsImage = function(){
		console.log("Calling itemsImage");
		var imgs = [
			"resources/images/flower.png",
			"resources/images/apple.png",
			"resources/images/candy.png",
			"resources/images/fish.png" ];
		var n = util.random( 0, imgs.length );
		return imgs[ n ];
	};
		
	///
	/// resetEventListening
	///
	this.resetEventListening = function(clusterI){
		/// Check for clusters collisions and splits
		clusterI.removeAllListeners('Moving');
		clusterI.removeAllListeners('SplitRequired');
		clusterI.removeAllListeners('Dropped');
		clusterI.on('Moving', bind(clusterI, function () {
			var sup = this.getSuperview();
			if( typeof sup !== 'undefined' )
			{
				sup.handleCollisions( this, sup.clusters );
				sup.handleWalls( this );
				sup.detectClusterOnPlate( this );
			};
			clusterI.isJoinable = sup.clusterIsOutOfContainer(clusterI);
		}));
		clusterI.on('SplitRequired', bind(clusterI, function() {
			var sup = this.getSuperview();
			if( typeof sup !== 'undefined' && this.isSplittable ){
				sup.splitCluster( this );
			}
		}));
		clusterI.on('Dropped', bind( clusterI, function(){
			var sup = this.getSuperview();
			if( typeof sup !== 'undefined' )
			{
				if( sup.detectClusterOnPlate(this) ){
					console.log("Cluster was dropped on plate");
					sup.handleClusterDroppedOnPlate(this);
				}
			}
		}));
	};
	
	///
	/// handleCollisions
	///
	/// Runs through all the objects in a given cluster list and check whether
	/// there is a collision between them and a given object. I such a collision
	/// exists, objects are joined together
	this.handleCollisions = function( cluster, clustersList ){
		/// Check whether 'cluster' collides with the rest of clusters
		var colliders = this.detectClusterCollision( cluster, clustersList).ret;
		if( !colliders.length ){
			/// Check if 'cluster' has splitted recently and is now stopped
			/// overlapping with its neighbors. If that's the case, make it
			/// joinable and splittable again.
			if( !cluster.isJoinable ){	
				cluster.isJoinable = true;
				cluster.isSplittable = true;
			}
			return;
		}
		/// If there are collisions, but cluster is not joinable, return.
		if( !cluster.isJoinable ){
			return;
		}
		this.sound.play('smash');
		if( cluster.movingOnInertia && cluster.views.length === 1 ){
			/// add cluster to colliders[0]
			colliders[0].addCluster(cluster,this.clustersPool);
			/// delete cluster
			var idx = this.clusters.indexOf(cluster);
			this.clusters.splice(idx,1);
			cluster.inertia.clear();
		}
		else{
			/// add colliders[0] to cluster
			cluster.addCluster(colliders[0],this.clustersPool);
			/// delete cluster
			var idx = this.clusters.indexOf(colliders[0]);
			this.clusters.splice(idx,1);
		}
	};
	
	///
	/// detectClusterCollision
	///
	/// Runs through all the objects in container and check which collides
	/// with the given object. 
	/// Returns a list with the objects that collide with the given object.
	this.detectClusterCollision = function( obj, container ){
		var colliders = [];
		if( container.length == 0 || !obj.isDraggable )
		{
			return {ret:colliders};
		}
		var r0,r1;
		r0 = obj.getBoundingShape();
		for( var i = 0; i<container.length; i++ )
		{
			r1 = container[i].getBoundingShape();
			if( obj != container[i] && intersect.rectAndRect(r0,r1) )
			{
				colliders.push( container[i] );
			}
		}
		return {ret:colliders};
	}
	
	///
	/// createSolutionDialog
	///
	this.createSolutionDialog = function(){
		/// Main solution view
		this.solutionDialog = new View({
			x: 0, y: 0, width: this.style.width, height: this.style.height,
			superview: this
		});
		/// Background, opaque view
		this.solutionDialogFog = new View({
			x: 0, y: 0, width: this.style.width, height: this.style.height,
			backgroundColor: "gray",
			opacity: 0.4,
			superview: this.solutionDialog
		});
		/// Smiley (sprite)
		var sw = this.solutionDialog.style.width*.3;
		this.solutionSmiley = new SpriteView({
			superview: this.solutionDialog,
			x: this.solutionDialog.style.width*.2,
			y: this.solutionDialog.style.height*.7 - sw*.5,
			width: sw, height: sw,
			zIndex: 2,
			anchorX: sw*.5,
			anchorY: sw*.5,
			r: 0,
			url: 'resources/images/animations/smile',
			frameRate: 15
		});
		
		this.solutionDialogFog.setHandleEvents(false);
		this.solutionSmiley.setHandleEvents(false);
		this.solutionDialog.hide();
		this.solutionDialog.on('InputStart',bind(this.solutionSmiley,function(){
			this.stopAnimation();
		}));
	};
	
	///
	/// showSolutionDialog
	///
	this.showSolutionDialog = function(solutionIsOK){
		this.solutionDialog.show();
		if( solutionIsOK ){
			this.solutionSmiley.startAnimation('updown',{loop:true});
			this.sound.play('chord');
		}else{
			this.solutionSmiley.startAnimation('leftright',{loop:true});
		}
	};

	///
	/// handleClusterDroppedOnPlate
	///
	this.handleClusterDroppedOnPlate = function(cluster){
		this.pause(cluster);
		this.plateShadow.hide();
		var newScale = 1.5;
		w = this.style.width;
		if( cluster.style.width*newScale >= w ){
			newScale = w/cluster.style.width*.95;
		}
		/// Move cluster to the center of the plate
		var plateS = this.plate.style;
		var newPos = {x: plateS.x + plateS.width/2, y: plateS.y};
		/// Make sure the scaled cluster doesn't go out of the screen by the right side
		var tmp = w-newPos.x - cluster.style.width*newScale/2.;
		if( tmp < 0 ){
			newPos.x += tmp*1.01;
		}
		animate(cluster).now({
			x: newPos.x-cluster.style.width*newScale/2.,
			y: newPos.y,
			scale: newScale
		},500);
		/// Show cluster's label
		cluster.updateLabel();
		/// Show solution dialog
		var solutionIsOK = cluster.views.length === this.gameTargetN;
		this.showSolutionDialog( solutionIsOK );
		this.solutionDialog.once('InputStart',bind(this,function(){
			this.solutionDialog.hide();
			if( solutionIsOK ){
				this.play();
				this.plateShadow.hide();
			}else{
				this.resume();
				/// Hide cluster's label
				cluster.once('DragStart',bind(cluster,'hideLabel'));;
				/// Move the cluster out of the plate
				animate(cluster).now({
					y: cluster.style.y-cluster.style.height*1.5,
					scale: 1.
				},500).
				wait(300).
				then(bind(cluster,function(){this.hideLabel();})).
				then(bind(this.plateShadow,function(){this.hide();}));
			}
		}));
	};
	
	///
	/// detectClusterOnPlate
	///
	this.detectClusterOnPlate = function(cluster){
		var r0 = cluster.getBoundingShape();
		var p0 = r0.getCenter();
		var r1 = this.plate.getBoundingShape();
		//var intersection = intersect.rectAndRect( r0,r1 );
		var intersection = intersect.pointAndRect(p0,r1);
		if( intersection ){
			this.plateShadow.show();
		}else{
			this.plateShadow.hide();
		}
		return intersection;
	};
	
	///
	/// handleWalls
	///
	/// When a cluster touches the border of the "spread area", it
	/// no longer has inertia.
	this.handleWalls = function( cluster ){
		/// Screens bounding rect
		var A = this.getBoundingShape();
		/// Cluster's bounding rect
		var r1 = cluster.getBoundingShape();
		/// Screen's inner rect
		var r2 = new Rect( r1.width, r1.height, 
						A.width-2.*r1.width, A.height-2.*r1.height );
		if( !intersect.rectAndRect(r1,r2) ){
			animate(cluster).clear();
		}
	};
	
	///
	/// splitCluster
	///
	this.splitCluster = function( cluster/*, evt*/ ){
		if( ! cluster.isSplittable ) return;
		var touchOrig = cluster.touchPoint[0].x;
		var touchNew = cluster.touchPoint[1].x;
		if( touchNew < touchOrig ){ /// new touch is left from original
			var I = Math.floor(touchNew/cluster.views[0].style.width)+1;
			var side = 'left';
		}
		else{
			var I = Math.floor(touchNew/cluster.views[0].style.width);
			var side = 'right';
		}
		/// Resize cluster
		var N = cluster.views.length - I;
		if( side === 'right' ){
			var removedViews = cluster.views.slice(I);
			cluster.popNElements( N, side );
		}else{
			var removedViews = cluster.views.slice(0,I);
			cluster.popNElements( I, side );
		}
		this.sound.play('glass');
		/// Make the cluster non joinable for a while
		cluster.isJoinable = false;
		/// Create new cluster
		var newCluster = this.clustersPool.obtainView();
		newCluster.isDraggable = true;
		newCluster.views = [];
		newCluster.addViews( removedViews );
		newCluster.updateOpts({visible:true});
		this.clusters.push(newCluster);
		/// Update the dragOffset of the original cluster if spliced by the left.
		if( side === 'left' ){
			cluster.dragOffset.x -= I*cluster.views[0].style.width;
		}
		/// Let the new cluster listen form 'Moving' and 'SplitRequired'
		this.resetEventListening(newCluster);
		/// Separate clusters
		var w = cluster.views[0].style.width/4.;
		if( side === 'left' ){
			cluster.style.x += w;
			newCluster.style.x -= w;
		}else{
			cluster.style.x -= w;
			newCluster.style.x += w;
		}
	};
	
	///
	/// clusterIsOutOfContainer
	///
	this.clusterIsOutOfContainer = function(cluster){
		var r1 = cluster.getBoundingShape();
		var r2 = this.basket0.getBoundingShape();
		return ! intersect.rectAndRect(r1,r2);
	};

});

