import ui.View as View
import ui.ImageView as ImageView
import ui.resource.Image as Image
import device
import animate

var defaults = {
};

exports = Class( View, function(supr){
	this.init = function(opts){
		opts = merge(opts,defaults);
		supr(this, 'init', [opts]);
		this.build(opts);
	};
	
	this.build = function(opts){
		var w = opts.width/11.;
		var h = w*180/150.;
		this.style.update({
			offsetY: -h,
			height: h
		});
		/// Draw all digits
		this.digits = [];
		for( var i=0; i<10; i++ )
		{
			var d = new ImageView({
				superview: this,
				x: (i+.5)*w,
				y: 0,
				width: w,
				height: h,
				image: "resources/images/digits/"+i+"j.png",
			});
			this.digits.push( d );
			this.digits[i].on('InputStart',bind(this.digits[i],function(evt,pt){
				this.getSuperview()
					.emit('digitTouched', Math.round(this.style.x/this.style.width-.5));
			}));
		}
	};
	this.animateWave = function(){
		var t1 = 150, t2 = t1*3/2.;
		for( var i=0; i<this.digits.length; i++ ){
			animate(this.digits[i]).wait(i*t1)
				.then({offsetY: -this.style.height*.5}, t2, animate.easeOut)
				.then({offsetY: 0}, t2, animate.easeIn);
		}
	};
});
		

