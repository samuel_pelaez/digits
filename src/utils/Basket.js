import ui.ImageView as ImageView
import ui.View as View
import ui.resource.Image as Image;
var basket0Img = new Image({url: "resources/images/basket0.png"})
var basket1Img = new Image({url: "resources/images/basket1.png"})

exports = Class( View, function(supr) 
{
	this.init = function (opts) {
		opts = merge(opts, {
			x: 0, y: 0,
			width: 100, height: 100
		});
		
		supr(this, 'init', [opts]);
		
		this.build(opts);
	};
	
	this.build = function(opts)
	{
		var w = opts.width;
		var h = opts.height;
		
		var bottomImg = new ImageView
		({
			superview: this,
			x: 0, y: 0,
			width: w,
			height: h,
			image: basket0Img
		});
		
		var topImg = new ImageView
		({
			superview: this,
			x: 0, y: 0,
			width: w,
			height: h,
			image: basket1Img
		});
		
	};
});
