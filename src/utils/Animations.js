import animate;



var myclass = Class( function(){
	
	this.init = function (opts) {
		opts = merge(opts, {
		});
		
	};

	///
	/// dance
	///
	/// Make an object "dance" (maybe should be something more like "vibrate")
	this.dance = function( obj ){
		obj.style.anchorX = obj.style.width/2.;
		obj.style.anchorY = obj.style.height/2.;
		var t = 50;
		var angle = Math.PI/8.;
		var anim = animate(obj).now({
			r: angle
		},t ).then({
			r: -angle
		},2*t ).then({
			r: angle
		},2*t ).then({
			r: -angle
		},2*t ).then({
			r: angle
		},2*t ).then({
			r: -angle
		},2*t ).then({
			r: angle
		},2*t ).then({
			r: -angle
		},2*t ).then({
			r: 0.
		},t );
		return anim;
	};

});


var obj = new myclass();
exports = obj;
