import math.geom.Point as Point
import event.input.InputEvent as InputEvent;
import device;

/// Class Touches
/** This class contains an array with 10 "touches". Every one of these
 * touches is the Point where a touch event is occurring right now. This
 * class allows for proper monitoring of multitouch events.
 */
exports = Class( function() {
	this.init = function(){
		this.zero = new Point(0,0);
		this.touches = new Array(10); 
		this.events = new Array(10); 
		for( i=0; i<this.touches.length; i++ ){
			this.touches[i] = this.zero;
			this.events[i] = 0;
		}
	};
	this.addEvent = function (evt) {
		this.touches[evt.id+1] = evt.srcPoint;
		this.events[evt.id+1] = evt;
	};
	this.delEvent = function (evt) {
		this.touches[evt.id+1] = this.zero;
		this.events[evt.id+1] = 0;
	};
	this.numberOfTouches = function(){
		var n=0;
		for( var i=0; i<this.touches.length; i++ ){
			if( this.touches[i] !== this.zero ){
				n++;
			}
		}
		return n;
	}
	this.printCurrentTouches = function(){  //TODO: delete this function
		var msg = "Current touches: ";
		for( var i=0; i<this.touches.length; i++ ){
			if( this.touches[i] !== this.zero ){
				msg += i-1+",";
			}
		}
		console.log(msg);
	};
	
	/// Return the point that results from the average of all the active
	/// touch points. This routine is usefull when an element is being
	/// touched at more than one point.
	this.averageTouchPoint = function(){
		var n=0;
		var avg = new Point(0,0);
		for( var i=0; i<this.touches.length; i++ ){
			if( this.touches[i] !== this.zero ){
				n++;
				avg.add( this.touches[i] );
			}
		}
		if( n ){
			avg.scale(1./n);
		}
		return avg;
	};
	
	this.isPinchOut = function( dragEvt, delta ){
		if( this.numberOfTouches() < 2 ) return false;
		
		var avg = this.averageTouchPoint();
		var oldDist = new Point(dragEvt.srcPoint).subtract( delta ).subtract( avg ).getMagnitude();
		var newDist = new Point(dragEvt.srcPoint).subtract( avg ).getMagnitude();
		return newDist > oldDist && delta.getMagnitude() > 1.8;
	};
	
	/// Return the leftmost and rightmost input events.
	/// returns: [ [leftEvt,leftPt], [rightEvt,rightPt] ]
	this.leftrightTouchEvts = function(){
		var W = device.width;
		var H = device.height;
		var leftPt = new Point(W,H);
		var rightPt = new Point(-1,-1);
		var leftEvt = 0;
		var rightEvt = 0;
		for( var i=0; i<this.touches.length; i++ ){
			if( this.touches[i] !== this.zero ){
				if( this.touches[i].x < leftPt.x ){
					leftPt = new Point(this.touches[i].x,this.touches[i].y);
					leftEvt = this.events[i];
				}
				else if( this.touches[i].x > rightPt.x ){
					rightPt = new Point(this.touches[i].x,this.touches[i].y);
					rightEvt = this.events[i];
				}
			}
		}
		return [[leftEvt,leftPt],[rightEvt,rightPt]];
	}
});
	





















