import ui.ImageView as ImageView
import ui.View as View
import ui.ViewPool as ViewPool
import src.utils.Cluster2 as Cluster

var init_count = 20;

exports = Class( function(supr){
	this.init = function(opts){
		//supr(this, 'init', [opts]);
		this.clustersPool = new ViewPool({
			ctor: Cluster,
			initCount: init_count
		});
		this.imageViewsPool = new ViewPool({
			ctor: ImageView,
			initCount: init_count
		});
	};
	
	///
	/// getCluster
	///
	this.getCluster = function(){
		var c = this.clustersPool.obtainView();
		return c;
	};
	
	///
	/// getImageView
	///
	this.getImageView = function(){
		var i = this.imageViewsPool.obtainView();
		return i;
	};
	
	///
	/// delCluster
	///
	this.delCluster = function(c){
		c.removeFromSuperview();
		this.clustersPool.releaseView(c);
	};
	
	///
	/// delImageView
	///
	this.delImageView = function(i){
		i.removeFromSuperview();
		this.imageViewsPool.releaseView(i);
	};
	
});
