import ui.View;
import ui.ImageView as ImageView;
import ui.ParticleEngine as ParticleEngine;

exports = Class(ui.View, function (supr) {

	this.init = function (opts) {
		opts = merge(opts, {
			x: 0,
			y: 0,
			width: 1,
			height: 1,
			initCount: 4
		});
		this.nParts = 4;

		supr(this, 'init', [opts]);
		
		this.pEngine = new ParticleEngine({
			superview: this,
			width: 1,
			height: 1,
			initCount: 3
		});

		this.tick = function(dt){
			this.pEngine.runTick(dt);
		};
	};
	
	this.implode = function(img1,img2,img3,img4){
		/// Obtain particle array
		var parts = this.pEngine.obtainParticleArray(this.nParts);
		/// Set size and dimensions
		var W = this.getSuperview().style.width;
		var H = this.getSuperview().style.height;
		var w = W/3.;
		var h = H;
		var obj;
		// Left digit
		obj = parts[0];
		parts[0].x = 0.;
		parts[1].x = w;
		parts[2].x = 2*w;
		for( var i=0; i<3; i++ ){
			obj = parts[i];
			obj.y = 0.;
			obj.dy = -h*1.1;
			obj.ddy = h*1.3;
			obj.opacity = 1.;
			obj.dopacity = 5.;
			obj.ddopacity = -6;
			obj.width = w;
			obj.height = h;
			obj.ttl = 3000;
		}
		parts[3].x = w;
		parts[3].y = 0;
		parts[3].scale = 0.;
		parts[3].dscale = 3.9;
		parts[3].ddscale = -4.5;
		parts[3].opacity = 0.;
		parts[3].dopacity = 1;
		parts[3].ddopacity = 0;
		parts[3].delay = 1000;
		parts[3].ttl = 1500;
		parts[3].width = w;
		parts[3].height = h;
		/// Set images
		parts[0].image = img1;
		parts[1].image = img2;
		parts[2].image = img3;
		parts[3].image = img4;
		/// Emit particles
		this.pEngine.emitParticles(parts);
	};
	
	this.implode2 = function(img1,img2,img3,img4){
		var parts = this.pEngine.obtainParticleArray(this.nParts);
		var x = 60;//this.superview.width/3.;
		var dx = 0;
		var dy = 40;
		var ddy = 60;
		var obj = parts[0];
		for( var i=0; i<3; i++ ){
			obj = parts[i];
			obj.dy = -dy;
			obj.ddy = ddy;
			obj.width = 60;
			obj.height = 60;
			obj.dscale = .3;
			obj.ddscale = -.3,
			obj.ttl = 4000;
			obj.dopacity = 2;
			obj.ddopacity = -2;
		}
		/// Left image
		obj = parts[0];
		obj.x = -x;
		obj.dx = dx;
		obj.image = img1;
		/// Right image
		obj = parts[2];
		obj.x = x;
		obj.dx = -dx;
		obj.image = img3;
		/// Center image
		obj = parts[1];
		obj.image = img2;
		/// Emerging image
		obj = parts[3];
		obj.image = img4;
		obj.dy = -dy;
		obj.ddy = ddy/2;
		obj.ttl = 3000;
		obj.dscale = .3;
		obj.ddscale = -.3,
		obj.opacity = 0.3;
		obj.dopacity = -2;
		obj.ddopacity = 2;
		obj.width = 60;
		obj.height = 60;
		/// Emit particles
		this.pEngine.emitParticles(parts);
	};
	
});


