import animate;
import ui.resource.Image as Image
import ui.View as View
import ui.TextView as TextView
import math.geom.Vec2D as Vec2D
import src.utils.MyUtils as MyUtils
import src.utils.ClusterTypes as ClusterTypes
import src.utils.ClusterDefaultProperties as DefaultProperties
import src.utils.DraggPinchable as DraggPinchable



/**
 * Clusters must take care of their own reservoir releasing.
 */
exports = Class( DraggPinchable, function(supr){
	///
	/// init(opts)
	///
	/// Cass constructor.
	this.init = function( opts ){
		/// These lists keep track of the views that belong to this 
		/// cluster and their locations relative to cluster coordinates.
		this.views = [];
		this.clusterTypes = new ClusterTypes();
		
		/// Set default properties
		opts = merge(opts, {
			elements:[],
			nElements:0,
			reservoir: 'No reservoir provided to Cluster\'s constructor',
			field: 'If not provided, will be set to this.superview',
			type: this.clusterTypes.types['units'],
			isJoinable: DefaultProperties.def.isJoinable,
			isSplittable: DefaultProperties.def.isSplittable,
			isSpreadable: DefaultProperties.def.isSpreadable,
			isLabelVisible: DefaultProperties.def.isLabelVisible,
			isUpgradable: DefaultProperties.def.isUpgradable,
			isDowngradable: DefaultProperties.def.isDowngradable
		});
		supr(this, 'init', [opts]);
		this.elements = [];
		this.reservoir = opts.reservoir;
		this.type = opts.type;
		if( typeof(opts.field) === typeof('string') ){
			this.field = opts.superview;
		}else{
			this.field = opts.field;
		}
		this.isJoinable = opts.isJoinable;
		this.isSplittable = opts.isSplittable;
		this.isSpreadable = opts.isSpreadable;
		this.isLabelVisible = opts.isLabelVisible;
		this.isUpgradable = opts.isUpgradable;
		this.isDowngradable = opts.isDowngradable;
		this.isRelocating = false;
		
		/// If there are elements, add them
		for( var i=0; i<opts.elements.length; i++ ){
			this.addElement(opts.elements[i]);
		}
		/// Otherwise, if a cluster size is required (by nElements),
		/// create the corresponding elements.
		if( opts.elements.length === 0 && opts.nElements !== 0 ){
			for( var i=0; i<opts.nElements; i++ ){
				var img = this.reservoir.getImage();
				img.style.visible = true;
			}
			this.setImage( this.type.image );
			this.updateVisibleArea();
			this.updateStyleFromVisibleArea();
		}
		
		/// build other graphical elements
		this.build();
		
		/// listen to signals
		this.on('DeleteMe', bind(this,function(){
			this.reservoir.delCluster(this);
		}));
		
		this.on('MovingOnInertia', function(){
			this.emit('Moving');
		});
		
		this.on('LastFingerUp', bind(this,function(){
			/// wait for the last element to finish its animation
			if( typeof this.elements[this.nElements()-1] === 'undefined' ){
				return;
			}
			animate(this.elements[this.nElements()-1])
				.then(bind(this,function(){
					this.updateStyleFromVisibleArea();
				}));
		}));
		
		this.on('LongPress', bind(this,function(x,y){
			var n = Math.floor(x / this.style.width*this.nElements());
			if( n === this.nElements()-1 ) n--;
			this.split(n+1);
		}));
		
		this.on('DoubleClick', bind(this,function(){
			this.duplicate();
		}));
		
		this.on('Pinch', function(d){
			if( d > 1.2 ){
				this.spread();
			}
		});
		
		this.onRelease = function(){
			this.removeAllListeners('Moving');
			for( var i=0; i<this.nElements(); i++ ){
				this.reservoir.delImageView(this.elements[i]);
			}
			this.elements = [];
		};
		
	};
	
	///
	/// build()
	///
	this.build = function(){
		/// visibleArea
		///
		/// This rect is used to detect intersection between clusters. 
		/// It is usually identical to bounding box, except when the 
		/// cluster is being dragged and its size has changed due to a
		/// collision. In those cases, the visibleArea is updated to the
		/// real new size of the cluster, while the cluster itself does
		/// not update its size so that it doesn't interfere with the
		/// dragging process.
		this.visibleArea = new View({
			superview: this,
			x: 0, y: 0,
			width: 0, height: 0,
		});
		
		this.label = new TextView({
			superview: this,//.visibleArea,
			width: GLOBAL.BOUNDS_WIDTH/10.,
			height: GLOBAL.BOUNDS_WIDTH/10.,
			backgroundColor: 'gray',
			color: 'white',
			zIndex: 1,
			autoFontSize: true,
			autosize: true,
			//wrap: true,
			visible: false,
		});
	};
	
	///
	/// addCluster(c)
	///
	this.addCluster = function(c){
		/// Accept only clusters of my same type
		if( c.type.image.getURL() !== this.type.image.getURL() ){
			return false;
		}
		/// Check that cluster is "joinable"
		if( !this.isJoinable || !c.isJoinable ){
			return false;
		}
		/// Run through all elements of c, adding them here
		var N = c.nElements();
		for( var i=N-1; i>=0; i-- ){
			if( this.addElement(c.elements[i])){
				c.removeElement(c.elements[i]);
				if( this.elements.length === 1 ){
					break;
				}
			}else{
				break;
			}
		}
		animate(this.elements[this.nElements()-1])
			.then(bind(this,function(){
				animate(this).then(bind(this,function(){
					if( this.nTouches < 1 ){
						this.updateStyleFromVisibleArea();
					}
				}));
			}));
		return true;
	};
	
	///
	/// addElement(e)
	///
	this.addElement = function(e,animation){
		var N = this.nElements();
		if( N === 0 ){
			/// If this is the first element
			/// set cluster's position and size as that ofthe new element
			var pos = e.getPosition(this.getSuperview());
			this.style.update( {x:pos.x, y:pos.y, width:pos.width, height:pos.height} );
			this.addSubview(e);
			e.style.update({x:0,y:0});
			this.elements.push(e);
			this.updateVisibleArea();
		}else if( N < this.type.nMaxElements ){
			var pos = e.getPosition(this.getSuperview());
			/// set e's position with respect to this cluster
			pos.x -= this.style.x;
			pos.y -= this.style.y;
			e.style.visible = false;
			e.style.update( {x:pos.x, y:pos.y, width:this.elements[0].style.width, height:this.elements[0].style.height} );
			this.addSubview(e);
			e.style.visible = true;
			this.elements.push(e);
			this.updateVisibleArea();
			/// animate e towards its new coordinate in this cluster
			var x = this.elements[0].style.x+ this.elements[0].style.width * (N * this.type.newElementCoordinates.x);
			var y = this.elements[0].style.y+ this.elements[0].style.height * (N * this.type.newElementCoordinates.y);
			if( animation || typeof animation === 'undefined' ){
				animate(e).now( {x: x, y: y}, 300, animate.easeOut);
				if( this.nElements() === this.type.nMaxElements ){
					this.upgradeType();
				}
			}else{
				e.style.update({ x:x, y:y });
			}
		}else{
			return false;
		}
		this.updateLabel();
		return true;
	};
	
	///
	/// removeElement(e)
	///
	this.removeElement = function(e){
		/// Remove element from elements array
		var idx = this.elements.indexOf(e);
		this.elements.splice(idx,1);
		//animate(e).clear();
		/// If this was the last element, delete me
		if( this.nElements() === 0 ){
			this.field.delCluster(this);
			this.reservoir.delCluster(this);
			return;
		}
		this.updateVisibleArea();
		this.updateStyleFromVisibleArea();
		this.updateLabel();
	};
	
	///
	/// createElement()
	///
	/// Creates an element (from the reservoir) and puts it in my current
	/// position
	this.createElement = function(){
		var e = this.reservoir.getImageView();
		e.updateOpts({
			superview: this.getSuperview(),
			x: this.style.x, y: this.style.y,
			width: this.style.width, height: this.style.height,
			visible: true
		});
		e.setImage( this.type.image );
		this.addElement(e);
	};
		
	///
	/// nElements()
	///
	this.nElements = function(){
		return this.elements.length;
	};
	
	///
	/// updateVisibleArea()
	///
	this.updateVisibleArea = function(){
		/// in some weird cases, this function is called even if no
		/// elements are in the cluster. For those cases, I make this:
		var N = this.nElements();
		if( N === 0 ){
			this.visibleArea.style.update({
				x:0, y:0, width:0, height:0
			});
			return;
		}
		/// update position and size of visibleArea
		var boc = this.type.boundingOppositeCorners;
		var p1 = this.elements[0].style.copy();
		var p2 = {
			x:p1.x + p1.width * (N-1) * this.type.newElementCoordinates.x,
			y:p1.y + p1.height* (N-1) * this.type.newElementCoordinates.y,
			width: p1.width, height: p1.height
		};
		p1.x += p1.width * boc.x1;		p1.y += p1.height* boc.y1;
		p2.x += p2.width * boc.x2;		p2.y += p2.height* boc.y2;
		var newStyle = {
			x: Math.min(p1.x,p2.x),
			y: Math.min(p1.y,p2.y),
			width: Math.abs(p2.x-p1.x),
			height: Math.abs(p2.y-p1.y)
		};
		this.visibleArea.style.update( newStyle );
	};
	
	///
	/// updateStyleFromVisibleArea()
	///
	this.updateStyleFromVisibleArea = function(){
		var d = this.visibleArea.style.copy();
		this.style.x += d.x;
		this.style.y += d.y;
		this.style.width = d.width;
		this.style.height = d.height;
		this.vel.x =0;
		this.vel.y =0;
		this.visibleArea.style.update({x:0,y:0});
		for( var i=0; i<this.nElements(); i++ ){
			var e = this.elements[i];
			e.style.x -= d.x;
			e.style.y -= d.y;
		}
	};
	
	///
	/// setImage(img)
	///
	this.setImage = function(img){
		var N = this.nElements();
		for( var i=0; i<N; i++ ){
			this.elements[i].setImage(img);
		}
	};
	
	///
	/// split(n)
	///
	/// Splits this cluster in two: one with n elements and the other 
	/// with the other N-n elements.
	this.split = function(n){
		var N = this.nElements();
		if( n<=0 || n>=N )return;
		/// Create two new clusters
		var c1 = this.reservoir.getCluster();
		var c2 = this.reservoir.getCluster();
		c1.type = this.type;
		c2.type = this.type;
		this.field.addSubview(c1);
		this.field.addSubview(c2);
		/// Add the elements to the new clusters
		for( var i=0; i<n; i++ ){
			var e = this.elements[i];
			c1.addElement(e,false);
		}
		for( var i=n; i<N; i++ ){
			var e = this.elements[i];
			c2.addElement(e,false);
		}
		c1.style.visible = true;
		c2.style.visible = true;
		/// add new clusters to field
		this.field.addCluster(c1);
		this.field.addCluster(c2);
		/// delete this cluster
		this.elements = [];
		this.reservoir.delCluster(this);
		this.field.delCluster(this);
		/// separate clusters
		c1.isJoinable = false;
		c2.isJoinable = false;
		var x2 = 0, y2 = 0; /// aux variable: position of c2 after separation
		if( this.type.duplicateDistance.x > 0 ){
			x2 = c1.style.x + c1.visibleArea.style.width + c1.elements[0].style.width;
		}else if( this.type.duplicateDistance.x < 0 ){
			x2 = c1.style.x - c2.visibleArea.style.width - c1.elements[0].style.width;
		}
		if( this.type.duplicateDistance.y > 0 ){
			y2 = c1.style.y + c1.visibleArea.style.height + c1.elements[0].style.height;
		}else if( this.type.duplicateDistance.y < 0 ){
			y2 = c1.style.y - c2.visibleArea.style.height - c1.elements[0].style.height;
		}
		var dx = 0; var dy = 0; /// actual displacement 
		dx = x2 ? (x2 - c2.style.x)/2. : 0;
		dy = y2 ? (y2 - c2.style.y)/2. : 0;
		var t = 200;		
		animate(c1).now({x:c1.style.x - dx, y:c1.style.y - dy },t,animate.easeOut);
		animate(c2).now({x:c2.style.x + dx, y:c2.style.y + dy },t,animate.easeOut);
		setTimeout( function(){
			c1.isJoinable = true;
			c2.isJoinable = true;
			c1.field.removeOutOfWallsClusters();
		},t*1.3);
		/// update size of new clusters
		c1.updateStyleFromVisibleArea();
		c2.updateStyleFromVisibleArea();
	};
	
	///
	/// spread()
	///
	this.spread = function(){
		var N = this.nElements();
		var Nmax = this.type.spreadDistribution.length;
		if( N < 1 ) return;
		if( N === 1 ){
			this.downgradeType();
			return;
		}
		if( N > Nmax ){
			this.split(Nmax);
			return;
		}
		var s = this.style.copy();
		var w = this.elements[0].style.width+0;
		var h = this.elements[0].style.height+0;
		var W = this.field.livingArea.width;
		var H = this.field.livingArea.height;
		var margin = Math.min(h,w)*.2;
		/// Check that no element gets out after the spread
		var arrayx = [], arrayy = [];
		for( var i=0; i<N; i++ ){
			arrayx.push(this.type.spreadDistribution[i][0]);
			arrayy.push(this.type.spreadDistribution[i][1]);
		}
		var xmin = s.x + w * Math.min.apply(Math, arrayx);///the left-most position after spread
		var ymin = s.y + h * Math.min.apply(Math, arrayy);///the top-most position 
		var xmax = s.x + w * Math.max.apply(Math, arrayx) + w;///the right-most position 
		var ymax = s.y + h * Math.max.apply(Math, arrayy) + h;///the bottom-most position 
		if( xmin < 0. ) s.x -= xmin - margin;
		if( ymin < 0. ) s.y -= ymin - margin ;
		if( xmax > W ) s.x -= (xmax-W) + margin;
		if( ymax > H ) s.y -= (ymax-H) + margin;
		this.style.update(s);
		/// Create clusters to spread
		for( var i=0; i<N; i++ ){
			var c1 = this.reservoir.getCluster();
			var e = this.elements[N-i-1];
			c1.type = this.type;
			this.field.addSubview(c1);
			c1.addElement(e);
			c1.style.visible = true;
			this.field.addCluster(c1,false);
			c1.isJoinable = false;
			var t = 200;
			c1.isRelocating = true;
			animate(c1).now({
				x:s.x + w * this.type.spreadDistribution[i][0], 
				y:s.y + h * this.type.spreadDistribution[i][1], 
			},t,animate.easeOut);
			setTimeout( bind(c1,function(){
				this.isJoinable = true;
				this.isRelocating = false;
				c1.field.removeOutOfWallsClusters();
			}),t*1.3);
			c1.updateStyleFromVisibleArea();
		}
		/// delete this cluster
		this.elements = [];
		this.reservoir.delCluster(this);
		this.field.delCluster(this);
		this.field.removeOutOfWallsClusters();
	};
	
	///
	/// duplicate()
	///
	this.duplicate = function(){
		var N = this.nElements();
		/// Create new cluster
		var c1 = this.reservoir.getCluster();
		c1.type = this.type;
		this.field.addSubview(c1);
		c1.style.update({
			superview: this.getSuperview(),
			x:this.style.x, y:this.style.y
		})
		/// Create and add elements to the new cluster
		for( var i=0; i<N; i++ ){
			/// Get new element
			var e = this.reservoir.getImageView();
			e.updateOpts({
				superview: this.getSuperview(),
				x: this.style.x, y: this.style.y,
				width: this.elements[0].style.width, height: this.elements[0].style.height,
				visible: true
			});
			e.setImage( this.type.image );
			c1.addElement(e,false);
		}
		c1.style.visible = true;
		this.field.addCluster(c1);
		c1.isJoinable = false;
		/// Displace the new cluster
		var x1 = 0, y1 = 0; /// aux variable: position of c1 after separation
		var s1 = c1.visibleArea.style.copy();
		var W = this.field.livingArea.width, H = this.field.livingArea.height;
		if( this.type.duplicateDistance.x > 0 ){
			x1 = this.style.x + this.visibleArea.style.width + this.elements[0].style.width;
			if( x1 + this.elements[0].style.width*1.1 > W ) /// Check that it won't be out of the field
				x1 = this.style.x - c1.visibleArea.style.width - this.elements[0].style.width;
		}else if( this.type.duplicateDistance.x < 0 ){
			x1 = this.style.x - c1.visibleArea.style.width - this.elements[0].style.width;
			if( x1 + s1.width - this.elements[0].style.width < 0 ) /// Check that it won't be out of the field
				x1 = this.style.x + this.visibleArea.style.width + this.elements[0].style.width;
		}
		if( this.type.duplicateDistance.y > 0 ){
			y1 = this.style.y + this.visibleArea.style.height + this.elements[0].style.height;
			if( y1 + this.elements[0].style.height*1.1 > H ) /// Check that it won't be out of the field
				y1 = this.style.y - c1.visibleArea.style.height - this.elements[0].style.height;
		}else if( this.type.duplicateDistance.y < 0 ){
			y1 = this.style.y - c1.visibleArea.style.height - this.elements[0].style.height;
			if( y1 + s1.height - this.elements[0].style.height < 0 ) /// Check that it won't be out of the field
				y1 = this.style.y + this.visibleArea.style.height + this.elements[0].style.height;
		}
		var dx = 0; var dy = 0; /// actual displacement 
		dx = x1 ? (x1 - c1.style.x) : 0;
		dy = y1 ? (y1 - c1.style.y) : 0;
		var t = 200;
		animate(c1).now({x:c1.style.x + dx, y:c1.style.y + dy },t,animate.easeOut);
		setTimeout( function(){
			c1.isJoinable = true;
			c1.field.removeOutOfWallsClusters();
		},t*1.3);
		c1.updateStyleFromVisibleArea();
	};
	
	///
	/// setType()
	///
	/// Update position and size of all elements according to the given type
	this.setType = function(type){
		var N = this.nElements();
		this.style.opacity = 0;
		var newType = this.clusterTypes.types[type];
		this.type = newType;
		var e0 = this.elements[0];
		var size = this.field.defaultClusterSize(type);
		var style = {x:0, y:0, width:size.width, height:size.height};
		for( var i=0; i<N; i++ ){
			var e = this.elements[i];
			style.x = e0.style.x + i*size.width*newType.newElementCoordinates.x;
			style.y = e0.style.y + i*size.height*newType.newElementCoordinates.y;
			this.elements[i].style.update(style);
			this.elements[i].setImage( newType.image );
		}
		this.updateVisibleArea();
		this.updateStyleFromVisibleArea();
		animate(this).clear().now({opacity:1},200);
	};
	
	///
	/// upgradeType()
	///
	/// If the number of elements is equal to the maximum, change the type 
	/// of cluster to the corresponding superType and make the number of
	/// elements be one.
	this.upgradeType = function(){
		if( this.type.superType === 'none' || !this.isUpgradable ) return;
		var N = this.nElements();
		if( N !== this.type.nMaxElements ) return;
		var newType = this.type.superType;
		if( newType.superType === 'none' ) return;
		for( var i=N-1; i>0; i-- ){
			var e = this.elements[i];
			this.removeElement(e);
			this.reservoir.delImageView(e);
		}
		this.setType(newType);
		this.field.updateCounter();
	};
	
	///
	/// downgradeType()
	///
	/// If the number of elements is one, and the cluster is pinched out,
	/// its type changes to a lower level and it spreads out
	this.downgradeType = function(){
		if( this.type.subType === 'none' || !this.isDowngradable ) return;
		var N = this.nElements();
		if( N !== 1 ) return;
		var newType = this.type.subType;
		this.setType(newType);
		/// Create and add elements to the new cluster
		for( var i=1; i<this.type.nMaxElements; i++ ){
			/// Get new element
			var e = this.reservoir.getImageView();
			e.updateOpts({
				superview: this.getSuperview(),
				x: this.style.x, y: this.style.y,
				width: this.elements[0].style.width, height: this.elements[0].style.height,
				visible: true
			});
			e.setImage( this.type.image );
			this.addElement(e,false);
		}
		this.spread();
		this.field.updateCounter();
	};
	
	///
	/// updateLabel()
	///
	this.updateLabel = function(){
		if( typeof this.field !== 'undefined' ) {
			this.field.updateCounter();
		}
		var N = this.nElements()*this.type.quantity;
		if( N > 1 && this.isLabelVisible ){
			this.label.setText(N.toString());
			this.label.style.x = (this.visibleArea.style.width - this.label.style.width)/2.
			this.label.style.y = (this.visibleArea.style.height - this.label.style.height)/2.
			this.label.show();
		}else{
			this.label.hide();
		}
	};
	
});		
		
