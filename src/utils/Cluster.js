import animate;
import ui.ImageView as ImageView
import ui.resource.Image as Image
import ui.View as View
import ui.ViewPool as ViewPool
import math.geom.Point as Point
import src.utils.MyUtils as MyUtils;
import src.utils.DraggPinchable as DraggPinchable
import src.utils.LabelAnimator as LabelAnimator;
import AudioManager;

exports = Class( DraggPinchable, function(supr){
	///
	/// init(opts)
	///
	/// Cass constructor.
	this.init = function( opts ){
		/// These lists keep track of the views that belong to this 
		/// cluster and their locations relative to cluster coordinates.
		this.views = [];
		
		/// Set default properties
		opts = merge(opts, {
			views:[],
			isSizeLableVisible: false,
			isSplittable: true,
			isJoinable: true,
			cubesEnabled: true
		});
		supr(this,"init", [opts]);
		
		//this.style.backgroundColor="green";
		//this.style.opacity = 0.5;
		
		this.sound = new AudioManager({
			path: "resources/audio/",
			files: {
				birds: {volume: 0.9},
				chord: {volume: 0.9},
				rattle: {volume: 0.9},
				glass: {volume: 0.9},
				smash: {volume: 0.9}
			}
		});
		
		/// Other default properties
		this.isSizeLableVisible = opts.isSizeLableVisible;
		this.isSplittable = opts.isSplittable;
		this.isJoinable = opts.isJoinable;
		this.touchPoint = [new Point(0,0), new Point(0,0)];
		
		this.build();

		/// If a list of views is passed in the options, add those views
		/// to the cluster
		this.views = [];
		for( var i=0; i<opts.views.length; i++ ){
			this.addView( opts.views[i] );
		}
		var v = this.views[0];
		
		/// When a cluster is dragged, all the views that belong to it
		/// must move with it
		this.on('Drag', function (startEvt, dragEvt, delta) {
			this.emit('Moving');
		});
		
		/// When a cluster is moving due to its inertia, all the views that
		/// belong to it must move with it
		this.on('MovingOnInertia', function(){
			if( this.enteringAnimation ) return;
			this.emit('Moving');
		});
		
		this.on('InputStart', function(evt,pt){
			if( this.nTouches === 1 )
				this.touchPoint[0] = new Point(pt);
			if( this.nTouches === 2 )
				this.touchPoint[1] = new Point(pt);
		});
		
		/// Detect pinch-out
		this.on('Pinch', function(d){
			if( this.isPinchable && d > 1.1 ){
				this.emit('SplitRequired');
				this.isPinchable = false;
			}
		});
		
		/// Detect when the cluster is dropped
		this.on('FingerUp', function(fingerCount){
			if( fingerCount === 0 ){
				this.emit('Dropped');
			}
		});
		
		this.onRelease = function(){
			this.removeAllSubviews();
		};
		this.onObtain = function(){
			this.isDraggable = true;
			this.isSplittable = true;
			this.isJoinable = true;
			this.allowInertia = true;
			this.views = [];
			this.cubesImage.style.visible = false;
			this.hideLabel();
			this.build();
			this.updateOpts({width: 0, height: 0});
		};
	};
	
	///
	/// build
	///
	this.build = function(){
		this.cubesEnabled = true;
		this.cubesImage = new ImageView({
			superview: this,
			visible: false
		});
	};
	
	///
	/// updateLabel
	///
	/// Set the label's image according to the number of views of the
	/// cluster.
	/// Args: 
	/// 	from1, from2: Number of elements of left and right clusters
	///		when this cluster is formed from the union of two smaller
	///		ones.
	this.updateLabel = function(from1,from2){
		this.isSizeLableVisible = true;
		if( typeof this.label !== 'undefined' ){
			this.removeSubview(this.label);
		}
		var w = this.style.width;
		var h = this.style.height;
		this.label = new View({
			superview: this,
			image: digitUrl( this.views.length ),
			width: h*3,
			height: h,
			x: (w-h*3.)/2.,
			y: -h
		});
		this.labelImage = new ImageView({
			superview: this.label,
			x: h*3/2.-h/2.,
			y: 0,
			width: h,
			height: h,
			image: digitUrl( this.views.length )
		});
		this.labelImage.style.opacity = 0.;
		var waitForAnimation = 0;
		if( from1 && from2 ){
			waitForAnimation = 1.;
			this.labelAnimator = new LabelAnimator({superview:this.label});
			this.labelAnimator.implode(
				digitUrl( from1 ),
				'resources/images/plus.png',
				digitUrl( from2 ),
				digitUrl( this.views.length )
			)
		}
		animate( this.labelImage ).
			wait(2000*waitForAnimation).
			then({opacity: 1.},1000);
	}
		
	///
	/// hideLabel
	///
	this.hideLabel = function(){
		this.isSizeLableVisible = false;
		if( typeof(this.label) !== 'undefined' ){
			this.label.hide();
		}
	}
	
	///
	/// addView(v)
	///
	/// Add a view to this cluster.
	/// The given view is moved towards the right of the cluster.
	this.addView = function( v, side ){
		if( side === undefined ) side = 'right';
		if( side !== 'right' && side !== 'left' ){
			console.log("In "+this
					+".addView(): Invalid parameter 'side' = "+side);
			return;
		}
		/// If this is the first object to be added to this cluster,
		/// copy its position and size
		var N = this.views.length;
		if( N == 0 ){
			var v_ = v.getPosition(this.getSuperview());
			this.style.x = v_.x;
			this.style.y = v_.y;
			this.style.width = v_.width;
			this.style.height = v_.height;
			v.style.x = 0;
			v.style.y = 0;
			v.removeFromSuperview();
			this.addSubview(v);
		}
		/// Otherwise, move the given view to the side of the cluster's 
		/// rightmost view. Also enhance the width of the cluster accordingly
		else{
			var posThis = this.getPosition();
			var posV = v.getPosition(this.getSuperview());
			var newPosV = {x:posV.x-posThis.x, y: posV.y-posThis.y};
			v.hide();
			v.removeFromSuperview();
			this.addSubview(v);
			v.style.update({x:newPosV.x, y:newPosV.y});
			v.show();
			animate(v).
				now({x:this.style.width, y:0},200);
			this.style.width += v.style.width;
			/*if( side === 'right' ){
				v.style.x = this.views[N-1].style.x + this.views[N-1].style.width;
				v.style.y = this.views[N-1].style.y;
				this.style.width += v.style.width;
			}else{ // side === 'left'
				v.style.x = 0;
				v.style.y = 0;
				this.style.x -= v.style.width;
				this.style.width += v.style.width;
				/// Displace all the old views to the right
				for( var i=0; i<this.views.length; i++ ){
					this.views[i].style.x += v.style.width;
				}
				this.dragOffset.x += v.style.width;
			}*/
		}
		
		this.views.push(v);
		/// Finaly, update cluster's lists, to keep track of the views 
		/// that belong to it
		/*if( side === 'right' ){
			this.views.push(v);
		}else{ // side === 'left'
			this.views.splice(0,0,v);
		}*/
		
		if( this.isSizeLableVisible ){
			this.updateLabel();
		}
		
		/// Add a semitransparent box to N>1 clusters
		animate(v).then( bind(this,function(){
			this.updateCubes();
		}));
	}
	
	///
	/// addViews
	///
	/// Add a bunch of views
	this.addViews = function( V, side ){
		var s1 = this.views.length;
		var s2 = V.length;
		var x1 = this.style.x + 0.5*this.style.width;
		var V0_ = V[0].getPosition(this.getSuperview());
		var V1_ = V[s2-1].getPosition(this.getSuperview());
		var x2 = 0.5*(V0_.x + V1_.x + V1_.width);
		
		/// Actually add the new views
		for( var i=0; i<V.length; i++ ){
			this.addView(V[i], side );
		}
		/// Also add some animations
		if( this.isSizeLableVisible ){
			if( x1 <= x2 ){
				this.updateLabel( s1,s2 );
			}else{
				this.updateLabel( s2,s1 );
			}
		}
	};
	
	///
	/// popNElements(N,side)
	///
	/// Shorten the cluster, popping out N elements on the given side.
	/// 	N: integer
	///		side: { 'right'|'left' } (default:'right')
	this.popNElements = function( N, side ){
		if( N <= 0 ) return
		if( side === undefined ) var side = 'right';
		if( side !== 'right' && side !== 'left' ){
			console.log("In function popNElements(N,side), wrong value "
				+ "of parameter: side '"+side+"'");
			return;
		}
		var newSize = this.views.length - N;
		if( side === 'right' ){
			/// Remove elements
			this.views.splice(newSize,N);
		}else{ //side === left
			/// Remove elements
			var removedViews = this.views.slice(0,N);
			this.views.splice(0,N);
			var v_ = this.views[0].getPosition(this.getSuperview());
			/// Update the cluster's position
			this.style.x = v_.x;
			/// Displace all the removed views to make sure they get out of 
			/// the cluster
			for( var i=0; i<N; i++ ){
				removedViews[i].style.x -= this.views[0].style.x;
			}
			/// Displace all the views to the left until the leftmost is at x=0.
			for( var i=this.views.length-1; i>=0; i-- ){
				this.views[i].style.x -= this.views[0].style.x;
			}
		}
		
		/// Update the cluster's width
		this.style.width = this.views[this.views.length-1].style.x
						+ this.views[this.views.length-1].style.width;
		/// Update the anchorPoint
		this.anchorPointX = this.style.width;
		/// Update the cluster's label
		if( this.isSizeLableVisible ){
			this.updateLabel();
		}
		/// Delete cubes
		this.updateCubes();
	};

	///
	/// updateCubes
	///
	/// Add a semitransparent box to N>1 clusters
	this.updateCubes = function(){
		if( !this.cubesEnabled ) return;
		var nviews = this.views.length
		if( this.cubesEnabled ){
			if( nviews > 1 && nviews <= 10 ){
				this.cubesImage.setImage(imgCube[nviews]);
				w = this.style.width; h = this.style.height;
				this.cubesImage.style.update({width:w, height:h});
				this.cubesImage.show();
			}else{
				this.cubesImage.hide();
			}
		}
	};
	
	///
	/// addCluster
	///
	/// Add another cluster's views to me.
	/// If pool is provided, the other cluster is released.
	this.addCluster = function(c,pool){
		this.addViews(c.views);
		c.removeAllListeners('Moving');
		c.removeAllListeners('SplitRequired');
		c.removeAllListeners('Dropped');
		c.isDraggable = false;
		//c.removeFromSuperview();
		if( typeof(pool)!=='undefined' ){
			pool.releaseView(c);
		};
	};
	
	
});


var images = [];
images.push( "resources/images/xifres/0j.png");
images.push( "resources/images/xifres/1j.png");
images.push( "resources/images/xifres/2j.png");
images.push( "resources/images/xifres/3j.png");
images.push( "resources/images/xifres/4j.png");
images.push( "resources/images/xifres/5j.png");
images.push( "resources/images/xifres/6j.png");
images.push( "resources/images/xifres/7j.png");
images.push( "resources/images/xifres/8j.png");
images.push( "resources/images/xifres/9j.png");

var imgCube = [
	new Image({url: "resources/images/cubes/cubes-U00.png"}),
	new Image({url: "resources/images/cubes/cubes-U01.png"}),
	new Image({url: "resources/images/cubes/cubes-U02.png"}),
	new Image({url: "resources/images/cubes/cubes-U03.png"}),
	new Image({url: "resources/images/cubes/cubes-U04.png"}),
	new Image({url: "resources/images/cubes/cubes-U05.png"}),
	new Image({url: "resources/images/cubes/cubes-U06.png"}),
	new Image({url: "resources/images/cubes/cubes-U07.png"}),
	new Image({url: "resources/images/cubes/cubes-U08.png"}),
	new Image({url: "resources/images/cubes/cubes-U09.png"}),
	new Image({url: "resources/images/cubes/cubes-U10.png"})
];

var digitUrl = function( indx ){
	return images[indx];
};


































