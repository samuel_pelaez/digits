import animate;
import math.geom.Point as Point


var myclass = Class( function(){
	this.init = function (opts) {
		opts = merge(opts, {
		});
	};
		
	/** Animate an object, moving it to the side of another object
	 * Params: 
	 *   obj1: the object to be moved
	 * 	 obj2: the fixed object, to whose side this obj1 will move
	 *   side: a string, either "left", "right", "top" or "bottom" (default: "right")
	 */
	this.joinToObject = function( obj1,obj2, side ){
		var p1 = new Point( obj2.style.x, obj2.style.y );
		
		// Move obj1 to the side of obj2
		var animDuration = 100;
		var animStyle = animate.linear;
		var anim = animate(obj1);
		console.log("moving "+obj1+" to the "+side+" of "+obj2);
		switch (side){
			case "left":
				anim.now( {x:obj2.style.x-obj1.style.width, 
									y:obj2.style.y}, animDuration, animStyle );
				break;
			case "top":
				anim.now( {x:obj2.style.x, 
									y:obj2.style.y-obj1.style.height}, animDuration, animStyle );
				break;
			case "bottom":
				anim.now( {x:obj2.style.x, 
									y:obj2.style.y+obj2.style.height}, animDuration, animStyle );
				break;
			default:  // right
				anim.now( {x:obj2.style.x+obj2.style.width, 
									y:obj2.style.y}, animDuration, animStyle );
				break;
		}
	}
	
	
	this.jump = function(view,heightPercet,duration){
		var s = view.style;
		animate(view).
			now({y:s.y-h/2.},duration/2,animate.linear).
			then({y:s.y},duration/2,animate.linear);
	}


});

var obj = new myclass();
//exports = obj;

exports.cleanPoolAndArray = function(pool,cluster){
	//console.log("MyUtils::cleanPoolAndCluster("+pool+","+cluster+")");
	if( typeof(cluster) === 'undefined' ) return;
	for( var i=0; i<cluster.length; i++ ){
		pool.releaseView(cluster[i]);
	}
	pool.releaseAllViews();
	cluster = [];
};

