import ui.resource.Image as Image

exports = Class(function(){
	this.init = function(opts){
		//opts = merge(opts,{});
		//supr(this, 'init', [opts]);
		
		this.types = {};
		this.types['units'] = {
			name: 'units',
			nMaxElements: 10,
			newElementCoordinates: { x:.93, y:0 },
			boundingOppositeCorners: { x1:0, y1:0, x2:1, y2:1 },
			//newElementCoordinates: { x:-1, y:0 },
			//boundingOppositeCorners: { x1:1, y1:0, x2:0, y2:1 },
			image: new Image({url:"resources/images/cubes/cubes-one.png"}),
			spreadDistribution: [
				[0,-.53],[0,.53],[1.5,-.53],[1.5,.53],[3,-.53],
				[3,.53],[4.5,-.53],[4.5,.53],[6,-.53],[6,.53]
			],
			duplicateDistance:{x:1, y:0},
			superType: 'tens',
			subType: 'none',
			quantity: 1
		};

		this.types['tens'] = {
			name: 'tens',
			nMaxElements: 10,
			newElementCoordinates: { x:-.009, y:.09 },
			boundingOppositeCorners: { x1:1, y1:0, x2:0, y2:1 },
			image: new Image({url:"resources/images/cubes/cubes-ten.png"}),
			spreadDistribution: [
				[-.51,-0],[.51,-0],[-.51,-1.1],[.51,-1.1],[-.51,1.1],
				[.51,1.1],[-.51,2.2],[.51,2.2],[-.51,-2.2],[.51,-2.2]
			],
			duplicateDistance:{x:0, y:1},
			superType: 'hundreds',
			subType: 'units',
			quantity: 10
		};

		this.types['hundreds'] = {
			name: 'hundreds',
			nMaxElements: 10,
			//newElementCoordinates: { x:0, y:-.43 },
			//boundingOppositeCorners: { x1:0, y1:1, x2:1, y2:0 },
			newElementCoordinates: { x:0, y:.43 },
			boundingOppositeCorners: { x1:0, y1:0, x2:1, y2:1 },
			image: new Image({url:"resources/images/cubes/cubes-hundred.png"}),
			spreadDistribution: [
				[-.51,-0],[.51,-0],[-.51,-1.1],[.51,-1.1],[-.51,1.1],
				[.51,1.1],[-.51,2.2],[.51,2.2],[-.51,-2.2],[.51,-2.2]
			],
			//duplicateDistance:{x:0, y:-1},
			duplicateDistance:{x:0, y:1},
			superType: 'thousands',
			subType: 'tens',
			quantity: 100
		};

		this.types['thousands'] = {
			name: 'thousands',
			nMaxElements: 10,
			newElementCoordinates: { x:.93, y:0 },
			boundingOppositeCorners: { x1:0, y1:0, x2:1, y2:1 },
			image: new Image({url:"resources/images/cubes/cubes-thousand.png"}),
			spreadDistribution: [
				[0,-.53],[0,.53],[1.5,-.53],[1.5,.53],[3,-.53],
				[3,.53],[4.5,-.53],[4.5,.53],[6,-.53],[6,.53]
			],
			duplicateDistance:{x:1, y:0},
			superType: 'none',
			subType: 'hundreds',
			quantity: 1000
		};
	};
});


