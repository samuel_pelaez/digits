import animate;
import ui.resource.Image as Image
import ui.ImageScaleView as ImageScaleView
import ui.View as View
import ui.ImageView as ImageView
import ui.GestureView as GestureView
import math.geom.Rect as Rect
import math.geom.intersect as intersect
import src.utils.MyUtils as MyUtils
import src.utils.Cluster2 as Cluster
import src.utils.ClusterTypes as ClusterTypes
import ui.widget.GridView as GridView
import ui.TextView as TextView

/**
 * The InteractionsField only cares about interactions between clusters,
 * so it doesn't take care of reservoir releasing of clusters. Clusters
 * must handle that themselves.
 */
exports = Class(GestureView, function(supr){
	this.init = function(opts){
		opts = merge(opts,{
			reservoir: 'No reservoir provided to InteractionsField\'s constructor',
			clusters: [],
			livingArea: 'this', /// ['this','screen']
			x: 0, y: 0,
			width: GLOBAL.baseWidth,
			height: GLOBAL.baseHeight,
			backgroundColor: '#FFCCCC',
			nClusters: 0,
			type: 'units'
		});
		supr(this, 'init', [opts]);
		this.clusters = opts.clusters;
		this.reservoir = opts.reservoir;
		this.livingAreaScheme = opts.livingArea;
		this.clusterTypes = new ClusterTypes();
		this.clusterType = opts.type;
		this._currentScale = this.style.scale;
		
		this.build();
		
		if( opts.nClusters ){
			this.createClustersAtRandomPos( opts.nClusters,
									this.clusterTypes.types[this.clusterType] );
		}
		
		this.on('Pinch',function(d){
			//this.style.scale = this._currentScale * (d);
			//this.buttonMove.style.scale = 1/this.style.scale;
		});
		this.on('ClearMulti', bind(this, 'clearMulti'));
		/*this.on('DragSingle', bind(this, 'drag'));
		this.drag = function(dx, dy) {
			this.style.x += dx;
			this.style.y += dy;
		};*/
		this.clearMulti = function(){
			this._currentScale = this.style.scale;
		};
	};
	
	///
	/// build()
	///
	this.build = function(){
		/// Set the size of the area where clusters live. If a cluster
		/// moves away from this area, it will be deleted.
		/// This area is defined with respect to this field's view.
		if( this.livingAreaScheme === 'this' ){
			this.livingArea = new Rect( 0, 0, 
								this.style.width, this.style.height);
		}else if( this.livingAreaScheme === 'screen' ){
			this.livingArea = new Rect( -this.style.x, -this.style.y, 
								GLOBAL.baseWidth, GLOBAL.baseHeight );
		}
		
		/// Button for cleaning the field
		var w = Math.min( GLOBAL.baseWidth, GLOBAL.baseHeight ) * 0.13;
		this.buttonClear = new ImageView({
			superview: this,
			x: this.style.width - w,
			y: 0,
			width: w, height: w,
			image: "resources/images/clean.png",
			zIndex: this.style.zIndex + 1,
		});
		/// Clean the field
		this.buttonClear.on('InputStart', bind(this,function(evt){
			this.deleteAllClusters();
		}));
		
		/// Counter
		this.counter = new Counter({
			superview: this,
			x: this.style.width/2,
			y: 0,
			zIndex: this.style.zIndex +1,
		});
		
		/// Button for moving the field
		this.buttonMove = new ImageView({
			superview: this,
			x: 0, y: 0,
			width: w, height: w,
			image: "resources/images/move.png",
			zIndex: this.style.zIndex + 1,
		});
		/// Button for resizing/scaling the field
		w = this.buttonMove.style.width;
		this.buttonResize = new ImageView({
			superview: this,
			x: this.style.width-w, y: this.style.height-w,
			width: w, height: w,
			image: "resources/images/resize.png",
			zIndex: this.style.zIndex + 1,
		});
		/// Handle moving and resizing of the field
		this.dragOffset = {x:this.buttonMove.style.width/2,y:this.buttonMove.style.width/2};
		this.superviewPos = this.getSuperview().getPosition();
		this.buttonMove.on('InputStart', bind(this,function(evt){
				this.draggingButtonMove = true;
				this.draggingButtonResize = false;
				this.startDrag({ inputStartEvt: evt, radius: 1 });
		}));
		this.buttonResize.on('InputStart', bind(this,function(evt){
				this.draggingButtonMove = false;
				this.draggingButtonResize = true;
				this.startDrag({ inputStartEvt: evt, radius: 1 });
		}));
		this.on('DragStart', function(dragEvt){
			if( this.draggingButtonMove || this.draggingButtonResize ){
				var scale = this.buttonMove.getPosition().scale;
				var absPos = this.getPosition();
				this.dragOffset.x = (dragEvt.srcPt.x - absPos.x)/scale;
				this.dragOffset.y = (dragEvt.srcPt.y - absPos.y)/scale;
				this.superviewPos = this.getSuperview().getPosition();
				this.style.opacity = .5;
			}
		});
		this.on('Drag', function(startEvt, dragEvt, delta){
			if( this.draggingButtonMove ){
				this.style.x = (dragEvt.point[1].x - this.superviewPos.x)/scale - this.dragOffset.x;
				this.style.y = (dragEvt.point[1].y - this.superviewPos.y)/scale - this.dragOffset.y;
			}
			if( this.draggingButtonResize ){
				var myscale = this.buttonResize.getPosition().scale;
				var absPos = this.getPosition();
				var s = (dragEvt.point[1].x - absPos.x)/(myscale*this.dragOffset.x);
				this.style.scale = this._currentScale * (s);
				this.buttonMove.style.scale = 1/this.style.scale;
				this.buttonResize.style.scale = 1/this.style.scale;
				this.buttonResize.style.x = this.style.width-this.buttonResize.style.width*this.buttonResize.style.scale;
				this.buttonResize.style.y = this.style.height-this.buttonResize.style.height*this.buttonResize.style.scale;
			}
		});
		this.on('DragStop', function(evt){
			this.draggingButtonMove = false;
			this.draggingButtonResize = false;
			this._currentScale = this.style.scale;
			this.style.opacity = 1;
		});
	};
	
	///
	/// defaultClusterSize(type)
	///
	this.defaultClusterSize = function(type){
		if( typeof type === 'undefined' )
			type = this.clusterType;
		/// Set the default size of clusters. It is defined in terms of
		/// the size of the livingArea.
		var s = Math.min(this.livingArea.width,this.livingArea.height);
		var size = {width:0, height:0};
		if( type === 'units' ){ size = {width: s*.1, height: s*.1}; }
		if( type === 'tens' ){ size = {width: s*.7, height: s*.08}; }
		if( type === 'hundreds' ){ size = {width: s*.55, height: s*.13}; }
		if( type === 'thousands' ){ size = {width: s*.25, height: s*.25}; }
		return size;
	};
	
	///
	/// addCluster(c)
	///
	this.addCluster = function(c){
		this.clusters.push(c);
		c.field = this;
		c.reservoir = this.reservoir;
		this.startMonitoring(c);
		this.updateCounter();
	};
	
	///
	/// delCluster(c)
	///
	this.delCluster = function(c){
		var idx = this.clusters.indexOf(c);
		this.clusters.splice(idx,1);
		this.stopMonitoring(c);
		this.updateCounter();
	};
	
	///
	/// deleteAllClusters
	///
	this.deleteAllClusters = function(){
		for( var i=this.clusters.length-1; i>=0; i-- ){
			var c = this.clusters[i];
			this.delCluster(c);
			this.reservoir.delCluster(c);
		}
	};
	
	///
	/// createCluster(pos,type)
	///
	this.createCluster = function(pos,type,N){
		if( typeof N === 'undefined' )N = 1;
		if( N <= 0 ) return;
		/// Create from reservoir
		var c1 = this.reservoir.getCluster();
		c1.type = type;
		this.addSubview(c1);
		c1.style.update({
			superview: this.field,
			x:pos.x, y:pos.y,
			width: this.defaultClusterSize(type.name).width,
			height: this.defaultClusterSize(type.name).height,
			scale:1
		});
		/// Create and add elements to the new cluster
		for( var i=0; i<N; i++ ){
			/// Get new element
			var e = this.reservoir.getImageView();
			e.updateOpts({
				superview: this,
				x: c1.style.x, y: c1.style.y,
				width: c1.style.width, height: c1.style.height,
				visible: true, scale: 1,
			});
			e.setImage( type.image );
			c1.addElement(e,false);
		}
		c1.style.visible = true;
		this.addCluster(c1);
		c1.updateStyleFromVisibleArea();
		return c1;
	};
	
	///
	/// createClustersAtRandomPos(N,type)
	///
	this.createClustersAtRandomPos = function(N,type){
		for( var i=0; i<N; i++ ){
			var pos = {
				x: Math.random()*(this.style.width
						-this.defaultClusterSize().width),
				y: Math.random()*(this.style.height
						-this.defaultClusterSize().height)
			}
			this.createCluster(pos,type);
		}
	};
	
	///
	/// startMonitoring(c)
	///
	this.startMonitoring = function(c){
		/// Check for clusters collisions and splits
		c.removeAllListeners('Moving');
		c.on('Moving', bind(c, function () {
			var sup = this.getSuperview();
			if( typeof sup !== 'undefined' )
			{
				sup.handleCollisions( this );
				sup.handleWalls( this );
			};
		}));
	};
	
	///
	/// stopMonitoring(c)
	///
	this.stopMonitoring = function(c){
		c.removeAllListeners('Moving');
	};
	
	///
	/// handleCollisions(c)
	///
	/// If cluster c collides with some other cluster, join them.
	this.handleCollisions = function( cluster ){
		/// Check whether 'cluster' collides with the rest of clusters
		var colliders = this.detectClusterColliders( cluster);
		if( !colliders.length ){
			/// Check if 'cluster' has splitted recently and is now stopped
			/// overlapping with its neighbors. If that's the case, make it
			/// joinable and splittable again. Of course, make also sure that
			/// the cluster is not in the middle of an animation!
			if( !cluster.isJoinable && !animate(cluster).hasFrames() ){	
				cluster.isJoinable = true;
				cluster.isSplittable = true;
			}
			return;
		}
		/// If cluster is not joinable, return.
		if( !cluster.isJoinable ){
			return;
		}
		for( var i=0; i<colliders.length; i++ ){
			//this.sound.play('smash');
			if( cluster.movingOnInertia && cluster.nElements() === 1 ){
				/// add cluster to colliders[i]
				colliders[i].addCluster(cluster);
			}
			else{
				/// add colliders[0] to cluster
				cluster.addCluster(colliders[i]);
			}
		}
		this.removeOutOfWallsClusters();
	};
	
	///
	/// detectClusterColliders(c)
	///
	/// Runs through all the objects in this field and check which 
	/// collides with the given object. 
	/// Returns a list with the objects that collide with the given 
	/// object.
	this.detectClusterColliders = function( c ){
		var colliders = [];
		if( this.clusters.length == 0 )
		{
			return colliders;
		}
		var Rc,Ri,Q;
		var pos_c = c.visibleArea.getPosition(this);
		Rc = new Rect(pos_c.x, pos_c.y, pos_c.width, pos_c.height);
		Q = c.type.quantity;
		Ri = new Rect(0,0,0,0);
		for( var i = 0; i<this.clusters.length; i++ )
		{
			var Ci = this.clusters[i];
			if( Ci.type.quantity !== Q )
				continue;
			//pos_ci = Ci.visibleArea.getPosition(this);
			//Ri = new Rect(pos_ci.x, pos_ci.y, pos_ci.width, pos_ci.height);
			Ri.x = Ci.style.x;	Ri.y = Ci.style.y;
			Ri.width = Ci.style.width; 	Ri.height = Ci.style.height;
			if( c != this.clusters[i] && intersect.rectAndRect(Rc,Ri) )
			{
				colliders.push( this.clusters[i] );
			}
		}
		return colliders;
	}
	
	///
	/// handleWalls
	///
	/// When a cluster goes out of the livingArea, it is deleted
	this.handleWalls = function(c){
		if( c.isRelocating ) return;
		if( !intersect.rectAndRect( c.getBoundingShape(), this.livingArea ) ){
			this.delCluster(c);
			this.reservoir.delCluster(c);
		}
	};
	
	///
	/// updateCounter
	///
	this.updateCounter = function(){
		var count = [0,0,0,0];
		for( var i=0; i<this.clusters.length; i++ ){
			var type = this.clusters[i].type.quantity;
			if( type === 1000 ){
				count[0] += this.clusters[i].nElements();
			}else if( type === 100 ){
				count[1] += this.clusters[i].nElements();
			}else if( type === 10 ){
				count[2] += this.clusters[i].nElements();
			}else if( type === 1 ){
				count[3] += this.clusters[i].nElements();
			}
		}
		this.counter.update(count);
	};
	
	///
	/// removeOutOfWallsClusters
	///
	this.removeOutOfWallsClusters = function(){
		for( var i=0; i<this.clusters.length; i++ ){
			var c = this.clusters[i];
			if( c.isRelocating ) continue;
			if( !intersect.rectAndRect( c.getBoundingShape(), this.livingArea ) ){
				this.delCluster(c);
				this.reservoir.delCluster(c);
			}
		}
		this.updateCounter();
	};
});

///
/// FramedText
///
/// A simple text box with a frame
var FramedText = Class(View,function(supr){
	this.init = function(opts){
		opts = merge(opts,{
			text: ' ',
			x: 0, y: 0,
			width: opts.superview.style.width,
			height: opts.superview.style.height,
		});
		supr(this,"init",[opts]);
		
		/// Make a frame from a 9slice scaled view
		this.frameImage = new Image({url: "resources/images/frame-simple-black.png"});
		this.frame = new ImageScaleView({
			superview: this, 
			width: this.style.width,
			height: this.style.height,
			image: this.frameImage, 
			scaleMethod: '9slice',
			sourceSlices: {
				horizontal:{left:10,center:44,right:10},
				vertical:{top:10,middle:44,bottom:10}},
			destSlices: {
				horizontal:{left:10,right:10},
				vertical:{top:10,bottom:10}},
		});
		/// Text box
		this.text = new TextView({
			superview: this,
			width: this.frame.style.width*.7,
			height: this.frame.style.height*.7,
			text: opts.text, 
		});
		this.text.style.x = (this.frame.style.width-this.text.style.width)/2.;
		this.text.style.y = (this.frame.style.height-this.text.style.height)/2.;
	};
});

var Counter = Class(View, function(supr){
	this.init = function (opts) {
		var w = GLOBAL.baseWidth *.2;
		opts = merge(opts, {
			x: 0, y: 0,
			width: w,
			height: w*.5,
			texts: ['m', 'c', 'd', 'u'],
		});
		supr(this,"init", [opts]);
		
		this.setHandleEvents(false,true);
		this.texts = opts.texts;
		this.box = [];
		
		var frame, zero = 0;
		for( var i=0; i<this.texts.length; i++ ){
			/// Characters
			frame = new FramedText({
				superview: this,
				x: this.style.width/this.texts.length*i,
				y: 0,
				width: this.style.width/this.texts.length*70/64,
				height: this.style.height*.4*70/64,
				text: this.texts[i],
			});
			/// Numbers
			this.box.push( new FramedText({
				superview: this,
				x: this.style.width/this.texts.length*i,
				y: this.style.height*.40,
				width: this.style.width/this.texts.length*70/64,
				height: this.style.height*.60,
				text: zero.toString(),
			}));
		}
	};
	
	/// update
	this.update = function(x){
		if( x.length !== this.box.length ){
			console.log("Warning! Updating counter with an array of the wrong size");
			return;
		}
		for( var i=0; i<x.length; i++ ){
			this.box[i].text.setText(x[i].toString());
		}
	};
});







