import ui.View as View
import ui.GestureView as GestureView
import math.geom.Point as Point
import animate


/// Class DraggPinchable
/** A View that can be dragged and/or pinched by touch input.
 *  It also has inertial movement when dragged and released.
*/
var defaults = {
	radius: 5,
	unbound: true,
	cover: false,
	isDraggable: true,
	isPinchable: true,
	allowInertia: true,
};
exports = Class(GestureView, function (supr) {
	
	
	/// Constructor
	this.init = function (opts) {
		opts = merge(opts, defaults);
		this.isDraggable = opts.isDraggable;
		this.isPinchable = opts.isPinchable;
		this.allowInertia = opts.allowInertia;
		this.dragOffset = {};
		this.nTouches = 0;
		this.vel = {x:0,y:0};
		
		supr(this,"init", [opts]);
		
		this.inertia = animate(this);
		this.doubleClicking = false;
				
		this.on("InputStart", bind(this, function (evt,pt) {
			/// When the object is touched, its inertial movement must stop.
			this.inertia.clear();
			/// Start drag
			if( this.isDraggable ){
				this.startDrag({
					inputStartEvt: evt,
					radius: opts.radius
				});
			}
			/// Start long press. Emit the position of the input event
			/// with respect to this view.
			var scale = this.getPosition().scale;
			var absPos = this.getPosition();
			var longPressX = (evt.srcPt.x - absPos.x)/scale;
			var longPressY = (evt.srcPt.y - absPos.y)/scale;
			this.withinLongPressInterval = true;
			this.longPress = setTimeout( bind(this,function(){ 
				this.emit("LongPress",longPressX,longPressY);
				this.withinLongPressInterval = false;
			}), 1000);
			/// Start double click
			if( this.doubleClicking && this._activeFingers === 1 ){
				this.doubleClicking = false;
				clearTimeout(this.longPress);
				this.withinLongPressInterval = false;
				this.emit('DoubleClick');
			}else{
				this.doubleClicking = true;
				setTimeout( bind(this,function(){
					this.doubleClicking = false;
				}), 400 );
			}
		}));
		
		this.on('DragStart', function (dragEvt) {
			if( this.getSuperview() === null || typeof this.getSuperview() === 'undefined'){
				return;
			}
			var scale = this.getPosition().scale;
			var absPos = this.getPosition();
			this.dragOffset.x = (dragEvt.srcPt.x - absPos.x)/scale;
			this.dragOffset.y = (dragEvt.srcPt.y - absPos.y)/scale;
			this.vel = {x:0,y:0};
			this.superviewPos = this.getSuperview().getPosition();
			//clearTimeout(this.longPress);
		});		
		this.on('Drag', function (startEvt, dragEvt, delta) {
			if( !this.isDraggable ) return;
			var scale = this.getPosition().scale;
			this.style.x = (dragEvt.point[1].x - this.superviewPos.x)/scale - this.dragOffset.x;
			this.style.y = (dragEvt.point[1].y - this.superviewPos.y)/scale - this.dragOffset.y;
			/// view is smaller than parent
			if (!opts.unbound) {
				var parent = this.getSuperview();
				this.style.x = Math.max(0, Math.min(this.style.x, parent.style.width - this.style.width));
				this.style.y = Math.max(0, Math.min(this.style.y, parent.style.height - this.style.height));
			}
			/// view is larger than parent
			if (opts.cover) {
				var parent = this.getSuperview();
				this.style.x = Math.min(0, Math.max(this.style.x, parent.style.width - this.style.width));
				this.style.y = Math.min(0, Math.max(this.style.y, parent.style.height - this.style.height));
			}
			var velScale = 1.;
			this.vel.x += delta.x*velScale;
			this.vel.y += delta.y*velScale;
			this.emit('Moving');
			if( this.withinLongPressInterval && (Math.abs(this.vel.x)+Math.abs(this.vel.y)) > opts.radius ){
				clearTimeout(this.longPress);
				this.withinLongPressInterval = false;
			}
		});
		
		this.on('FingerDown', function(fingerCount){
			this.nTouches = fingerCount;
			if( fingerCount > 1 ){
				this.isDraggable = false;
				if( this.withinLongPressInterval ){
					clearTimeout(this.longPress);
					this.withinLongPressInterval = false;
				}
			}
		});
		this.on('FingerUp', function(fingerCount){
			if( fingerCount < 1 ){
				this.emit('LastFingerUp');
			}
			this.nTouches = fingerCount;
			if( !this.isDraggable && fingerCount < 1 ){
				this.isDraggable = true;
				this.isPinchable = true;
			}else if( this.isDraggable && fingerCount < 1 ){
				this.startInertia();
			}
			if( this.withinLongPressInterval ){
				clearTimeout(this.longPress);
				this.withinLongPressInterval = false;
			}
		});
		
		
		this.tick = function(dt){
			if( this.inertia.hasFrames() ){
				this.emit('MovingOnInertia');
				this.movingOnInertia = true;
			}
			else{
				this.movingOnInertia = false;
			}
			/// Forget velocity. 
			/// The velocity gained during drag must decrease slowly if
			/// (while the user 
			this.vel.x *= .98;
			this.vel.y *= .98;
		};
		
		this.onRelease = function(){
			this._activeFingers = 0; /// from devkit/lib/timestep/src/ui/GestureView.js
		};
		this.onObtain = function(){
			this._activeFingers = 0; /// from devkit/lib/timestep/src/ui/GestureView.js
		};
	};
	
	///
	/// startInertia
	///
	/// Begin an inertial movement animation based on the view's current 
	/// velocity.
	this.startInertia = function(){
		if( !this.allowInertia )
			return;
		var vscale = 30;
		var tscale = 100.;
		var v = new Point(this.vel.x,this.vel.y);
		if( v.getMagnitude() < 0.1 ) return;
		var dest = new Point( this.style.x, this.style.y )
				.add( v.x * vscale, v.y * vscale);
		var duration = 1000;//+v.getMagnitude()*tscale;
		this.inertia = animate(this).
			then({x:dest.x, y:dest.y},duration,animate.easeOut);
	};
	
	
});

