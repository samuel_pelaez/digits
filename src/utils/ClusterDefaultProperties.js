

exports.def = {
	isJoinable: true,
	isSplittable: true,
	isSpreadable: true,
	isLabelVisible: true,
	isUpgradable: true,
	isDowngradable: true,
}
	
